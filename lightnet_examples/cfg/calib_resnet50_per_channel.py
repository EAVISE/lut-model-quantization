import lightnet as ln
import torch
import distiller
import distiller.models

# NOTE: use this file for the calibrate.py script

__all__ = ['params']

ROOT = 'data/imagenet/'
params = ln.engine.HyperParameters(
    # Network
    input_dimension = (224, 224),
    batch_size_test = 24,
    batch_size_calib = 50,

    calib_size = 50,

    # Dataset
    _valid_set = ROOT + 'val',

    # Preprocessing
    preprocessor_test = 'default_noaug',

    # Quantization
    quantize_inputs = True,
    bits_inputs = 8,
    signed_inputs = True,
    per_channel = True,
    bits_activations = 8,
    bits_weights = 8,
    bits_bias = 16,
    non_uniform_weights = False,
    weight_estimator = 'max',
    activation_estimator = 'max'
)

# Network
params.network = distiller.models.create_model(True, 'imagenet', 'resnet50', False, -1)

# Loss
params.loss = torch.nn.CrossEntropyLoss()
