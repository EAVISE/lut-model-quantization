import os
import time
import torch
import argparse
import distiller
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from lightnet.engine import Engine
from utils import from_file, prepare_logdir

# to fix issue https://stackoverflow.com/questions/27147300/matplotlib-tcl-asyncdelete-async-handler-deleted-by-the-wrong-thread
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class QuantizationAwareTrainingEngine(Engine):
    """Lightnet engine with common functionality for QAT process
    """

    def start(self):
        self.params.to(self.device)
        if self.valid_loader is not None:
            self.batch_end(1000)(self.test)
        self.batch_end(50)(self.plot_scalars)
        #self.batch_end(500)(self.plot_figures)

        self.froze_bn = False
        if self.freeze_bn_start == 0:
            self.freeze_batchnorm_cb()
        else:
            self.batch_end(self.freeze_bn_start)(self.freeze_batchnorm_cb)

        requires_prerun = False
        self.all_q_levels_are_frozen = False

        if hasattr(self, 'freeze_all_q_levels'):
            # for freezing all q levels at once at some iteration
            if self.freeze_all_q_levels == 0:
                self.freeze_all_q_levels_cb()
            else:
                requires_prerun = True
                self.batch_end(self.freeze_all_q_levels)(self.freeze_all_q_levels_cb)
        else:
            self.external_log.warning('freeze_all_q_levels not in cfg: q levels are not all frozen at once at some iteration')

        # TODO: rename thresholds to scales
        if hasattr(self, 'freeze_thresholds_start') and hasattr(self, 'freeze_thresholds_period'):
            self.batch_end(self.freeze_thresholds_period)(self.freeze_thresholds_cb)
        else:
            self.external_log.warning('freeze_thresholds_start and/or freeze_thresholds_period not in cfg: no threshold freeze procedure active')

        if hasattr(self, 'freeze_q_levels_start') and hasattr(self, 'freeze_q_levels_period'):
            # for freezing q levels gradually
            if self.freeze_q_levels_start == 0:
                self.freeze_all_q_levels_cb()
            else:
                requires_prerun = True
                self.batch_end(self.freeze_q_levels_period)(self.freeze_q_levels_cb)
        else:
            self.external_log.warning('freeze_q_levels_start and/or freeze_q_levels_period not in cfg: no gradual q level freeze procedure active')

        # prerun QEM
        #if requires_prerun:
        #    # prerun QEM to initialize q levels
        #    self.network.train()
        #    self.log('Pre-run model to initialize q levels using QEM')
        #    distiller.quantization.FakeNonLinearQEMQuantization.pre_run(self.network, self.device)
        #    self.network.eval()

        self.optimizer_weights.zero_grad()
        if hasattr(self, 'optimizer_thresh'):
            self.optimizer_thresh.zero_grad()
        else:
            self.external_log.warning('optimizer_thresh not in cfg: thresholds are not optimized')

        if not hasattr(self, 'aiwq_sampling_start') or not hasattr(self, 'aiwq_sampling_period'):
            self.aiwq_sampling_start = -1
            self.log('aiwq_sampling_start and/or aiwq_sampling_period not in cfg: aiwq sampling disabled')

        self.log(repr(self.network))

        self.task_start()

        self.plot_scalars()
        self.plot_figures()
        self.test()

    def task_start(self):
        """ Override in derived class in case task specific setup needs to occure
        """
        pass

    def train_batch(self):
        prev_batch = self.batch -1
        # We cannot call update_aiwq from a batch_end/batch_start periodic function since this call has to happen
        # AFTER the model forward and BEFORE the weight update
        if self.aiwq_sampling_start >= 0 \
                and prev_batch >= self.aiwq_sampling_start \
                and prev_batch % self.aiwq_sampling_period == 0:
            distiller.quantization.update_aiwq(self.network)

        self.optimizer_weights.step()
        self.optimizer_weights.zero_grad()
        self.scheduler_weights.step(self.batch, epoch=self.batch)
        if hasattr(self, 'optimizer_thresh'):
            self.optimizer_thresh.step()
            self.optimizer_thresh.zero_grad()
            self.scheduler_thresh.step(self.batch, epoch=self.batch)

        self.task_train_batch()

    def task_train_batch(self):
        """ Override in derived class in case task specific train operations are needed
        """
        pass

    def plot_scalars(self):
        self.writer.add_scalar('Train/WeightLearningRate', self.optimizer_weights.param_groups[0]['lr'], self.batch)
        if hasattr(self, 'optimizer_thresh'):
            self.writer.add_scalar('Train/ThresholdLearningRate', self.optimizer_thresh.param_groups[0]['lr'], self.batch)

        # log scale factors
        for name, module in self.network.named_modules():
            if isinstance(module, distiller.quantization.FakeLinearTrainedQuantization) or \
               isinstance(module, distiller.quantization.FakeLinearEstimatedQuantization):
                if module.per_channel:
                    # Log percentage of frozen channels
                    value = module.frozen_channels.sum() / float(len(module.frozen_channels))
                    self.writer.add_scalar(f'Frozen scale factors/{name}', value, self.batch)
                else:
                    # Log scale factors
                    value = module.scale.item()
                    self.writer.add_scalar(f'Scale factors/{name}', value, self.batch)

        self.task_plot_scalars()

    def task_plot_scalars(self):
        """ Override in derived class in case task specific scalars need plotting
        """
        pass

    def plot_figures(self):
        """Plot quantization figures in tensorboard using matplotlib
            * AIWQ sampling figure if enabled
            * Weight distributions with q levels if #bits per weight <= 5
        """

        # get a list of weights and quant module refs
        conv_info = []
        aiwq_metrics = []
        for name, module in self.network.named_modules():
            if isinstance(module, distiller.quantization.QConvLinear):
                conv_info.append((name, module.weight.cpu().numpy(), module.quant_weights))
                aiwq_metrics.append((name, module.aiwq.metric.item()))

        if self.aiwq_sampling_start >= 0:
            # generate AIWQ metrics figure
            labels = []
            bar_width = 1
            fig, ax = plt.subplots(figsize=(30, 5))
            for i, (name, metric) in enumerate(aiwq_metrics):
                ax.bar(i, metric)
                labels.append(name)
            ax.set_xticks(np.arange(len(labels)))
            ax.set_xticklabels(labels, rotation = 45)
            ax.set_yscale('log')
            fig.set_figwidth(bar_width * len(labels))
            self.writer.add_figure('AIWQ_metrics', fig, self.batch)

        # We are only interested in generating quantization level figures if weights are 5 bit and lower
        if not hasattr(self, 'bits_weights') or self.bits_weights is None or self.bits_weights > 5:
            return

        # generate matplotlib figures of weight distributions with quantization levels
        q_levels = None
        fig_height = 2
        fig, subplots = plt.subplots(len(conv_info), figsize=(10, fig_height))
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        for (name, weights, quant), ax in zip(conv_info, subplots):
            if hasattr(quant, 'q_levels'):
                q_levels = quant.q_levels.detach() / quant.num_bins     # convert to values in range [-1, 1]
                linear_quantizer = False
            else:
                q_levels = torch.arange(quant.quant_core.min_level, quant.quant_core.max_level+1, device=self.device)
                q_levels = q_levels / float(quant.quant_core.q_levels_per_sign) # convert to values in range [-1, 1]
                linear_quantizer = True

            if linear_quantizer and quant.per_channel:
                # plot max 3 filter distributions in the same figure with different colors
                scales = quant.scale
                num_filters_to_plot = min(len(weights), 3)
                ystep = 1/(num_filters_to_plot + 1.)
                for i in range(num_filters_to_plot):
                    color = colors[i]
                    ax.hist(weights[i].flatten(), histtype='stepfilled', alpha=0.3, bins=50, density=True, edgecolor='k', color=color)
                    scaled_q_levels = q_levels * scales[i]
                    for l in scaled_q_levels:
                        ax.axvline(x=l, ymin=((i+1)*ystep - ystep/2), ymax=((i+1)*ystep + ystep/2), color='k' if quant.frozen else color)
            else:
                scaled_q_levels = quant.scale * q_levels
                ax.hist(weights.flatten(), histtype='stepfilled', bins=50, density=True)
                color = 'C7' if quant.frozen else 'C3'
                for l in scaled_q_levels:
                    ax.axvline(x=l, color=color)
            ax.set_title(name)
        fig.set_figheight(fig_height * len(subplots))
        self.writer.add_figure('Weight dists and scaled Q levels', fig, self.batch)

    def freeze_batchnorm_cb(self):
        if not self.froze_bn:
            self.log("Freezing batchnorm statistics")
            distiller.quantization.set_frozen_state_batchnorm_stats(self.network, True)
            # one-time action
            self.froze_bn = True

    def freeze_thresholds_cb(self):
        # Incrementally freeze thresholds starting at N batches
        if self.batch < self.freeze_thresholds_start:
            return

        frozen_scales, num_frozen_scales, num_scales = distiller.quantization.freeze_quantization_scales(self.network)

        display_data = {}
        for name in frozen_scales:
            if name not in display_data:
                display_data[name] = 1
            else:
                display_data[name] += 1

        if len(frozen_scales) > 0:
            display_data_str = ", ".join([f"{k} ({v})" for k, v in display_data.items()])
            self.log(f'Froze {len(frozen_scales)} scales ({num_frozen_scales}/{num_scales}) in layers: {display_data_str}')

    def freeze_q_levels_cb(self):
        # Incrementally freeze q levels, similar as thresholds
        if self.batch < self.freeze_q_levels_start:
            return

        frozen_quantizers, num_frozen_quantizers, num_quantizers = distiller.quantization.FakeNonLinearQEMQuantization.freeze_stabilized_q_levels(self.network)

        if len(frozen_quantizers) > 0:
            self.log(f'Froze q levels in {frozen_quantizers[0]} ({num_frozen_quantizers}/{num_quantizers})')

    def freeze_all_q_levels_cb(self):
        # freeze all q levels at once (one time event)
        if not self.all_q_levels_are_frozen:
            distiller.quantization.set_frozen_state_non_linear_quantization_layers(self.network, True)
            self.log(f'Froze all q levels')
            self.all_q_levels_are_frozen = True

    def test(self):
        """Override in derived class
        """
        raise NotImplementedError

    def quit(self):
        return self.batch >= self.max_batches or self.sigint


def main(engine_type, train_dataset_type, valid_dataset_type, log, collate_fn=None):

    parser = argparse.ArgumentParser(description='Train network')
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-l', '--logdir', help='Logging folder', default='./logs')
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=8)
    parser.add_argument('-d', '--deterministic', action='store_true', help='Enable deterministic behavior')
    args = parser.parse_args()

    torch.set_num_threads(8)
    torch.set_num_interop_threads(8)

    # Parse generic arguments
    if args.deterministic:
        log.info('Deterministic enabled')
        distiller.set_deterministic()

    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda:0')
        else:
            log.error('CUDA not available')

    # Load parameters
    init_weight_file = [args.weight]    # make by reference variable
    parameters = from_file(args.network, weight_file=init_weight_file)

    # ensure parameters are iterable
    try:
        iter(parameters)
    except TypeError:
        parameters = [parameters]

    # Prepare logging directory
    tb_dir_name, checkpoints_dir_name = prepare_logdir(args.network, args.logdir, args.weight)

    eng = None

    # Iterate over multiple training sessions if any
    for stage, params in enumerate(parameters):

        if eng is not None and eng.sigint:
            res = ''
            while res != 'y' and res != 'n':
                print("Continue next iteration? (y/n)")
                res = input()
            if res == 'n':
                break

        # Dataloaders
        train_loader = torch.utils.data.DataLoader(
            train_dataset_type(params),
            batch_size = params.mini_batch_size,
            shuffle = True,
            drop_last = True,
            num_workers = args.workers,
            pin_memory = True,
            collate_fn = collate_fn
        )

        if params.valid_set is not None:
            valid_loader = torch.utils.data.DataLoader(
                valid_dataset_type(params),
                batch_size = params.mini_batch_size,
                shuffle = False,
                drop_last = False,
                num_workers = args.workers,
                pin_memory = True,
                collate_fn = collate_fn
            )
        else:
            valid_loader = None

        chkpt_weights_file = os.path.join(checkpoints_dir_name, f"best_{stage:02}.pt")

        # Start training
        eng = engine_type(
            params, train_loader,
            valid_loader=valid_loader,
            device=device,
            checkpoint_filename=chkpt_weights_file,
            writer=SummaryWriter(os.path.join(tb_dir_name, f"{stage:02}")),
            external_log=log,
        )
        b1 = eng.batch
        t1 = time.time()
        eng()
        t2 = time.time()
        b2 = eng.batch
        log.info(f'Training {b2-b1} batches took {t2-t1:.2f} seconds [{(t2-t1)/(b2-b1):.3f} sec/batch]')

        # Clear registered functions
        engine_type._batch_end = {}

        if os.path.exists(chkpt_weights_file):
            # Load weights from this training stage in the next one
            init_weight_file[0] = chkpt_weights_file
