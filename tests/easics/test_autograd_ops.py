import torch
import distiller.quantization.easics.autograd_ops as ops


class TestProjSteFn:
    def test_forward(self):
        x = torch.Tensor([1.4, -0.5, -1.0])
        q_levels = torch.Tensor([1.0, -0.75])
        q_res, idxs = ops.proj_ste_fn.apply(x, q_levels)

        assert torch.allclose(q_res, torch.Tensor([1.0, -0.75, -0.75]))

    def test_backward(self):
        x = torch.Tensor([1.4, -0.5, -1.0]).requires_grad_()
        q_levels = torch.Tensor([-0.75, 1.0])
        q_res, idxs = ops.proj_ste_fn.apply(x, q_levels)
        out = q_res.sum()
        out.backward()

        assert torch.allclose(x.grad, torch.Tensor([1.0, 1.0, 1.0]))
