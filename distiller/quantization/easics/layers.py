import math
import torch
import numpy as np
from torch.nn import functional as F
import torch.nn as nn
from tqdm import tqdm

from .utils import ExponentialMovingAverage, match_dims
from .autograd_ops import *


def set_frozen_state_non_linear_quantization_layers(model, state):
    """Freeze or unfreeze all non linear quantization layer parameters
    """
    FakeNonLinearQEMQuantization.set_frozen_state(model, state)

def freeze_quantization_scales(model):
    """
    Run all class methods of quantization layers that have a 'freeze_stabilized_scales' class method
    Returns:
    frozen_scales (list):       List of layer names that contain newly frozen scales
    num_frozen_scales (int):    Number of scales that are currently frozen in the whole model
    num_scales (int):           Total number of scales that the model contains
    """

    tot_frozen_scales = []
    tot_num_frozen_scales = 0
    tot_num_scales = 0

    frozen_scales, num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model)
    frozen_scales = [name for name, _, _, _ in frozen_scales]

    tot_frozen_scales += frozen_scales
    tot_num_frozen_scales += num_frozen_scales
    tot_num_scales += num_scales

    return frozen_scales, num_frozen_scales, num_scales


class FakeLinearSymmetricQuantCore(nn.Module):
    """Linear quantization core algorithm
    """
    def __init__(self, num_bits=8, is_signed=True, per_channel_dim=1, **kwargs):
        """
        num_bits (int):         Number of bits to quantize to
        is_signed (bool):       If False, use unsigned data type for quantized values
        per_channel_dim (int):  Indicates the per_channel dimension. For activation tensors, this is usually
                                1, for weight tensors this is usually 0 (=filter dim)
        """
        super(FakeLinearSymmetricQuantCore, self).__init__()

        self.num_bits = num_bits
        self.is_signed = is_signed
        self.per_channel_dim = per_channel_dim
        self.q_levels_per_sign = 2**(num_bits-int(is_signed))
        self.min_level = - self.q_levels_per_sign * int(is_signed)
        self.max_level = self.q_levels_per_sign - 1

    def forward(self, x, scale):
        """Apply the following quantize and de-quantize operation:
            out = s*clamp(round(x/s), n, p)
            s = scale / 2^(b - signed)
            n = -2^(b-1) if signed else 0
            p = 2^(b-1) -1 if signed else 2^b -1
        """
        # divide and change dimensions of scale to the dimensions of tensor x
        scale = match_dims(scale / self.q_levels_per_sign, x, dim=self.per_channel_dim)

        # quant
        x_quant = torch.clamp(round_ste_fn.apply(x / scale), self.min_level, self.max_level)

        # dequant
        return x_quant * scale

    def _repr_params(self):
        return f'num_bits={self.num_bits}, is_signed={self.is_signed}'

    def __repr__(self):
        return f'{self.__class__.__name__}(self._repr_params())'


class FakeLinearAsymmetricQuantCore(nn.Module):
    """Asymmetric Linear quantization core algorithm
    """
    def __init__(self, num_bits=8, **kwargs):
        """
        num_bits (int):         Number of bits to quantize to
        Note that per-channel is currently not supported for asymmetric quantization
        """
        super(FakeLinearAsymmetricQuantCore, self).__init__()

        self.num_bits = num_bits
        self.q_levels = 2**(num_bits)
        self.max_level = self.q_levels - 1

    def forward(self, x, scale, zero):
        """Apply quantization
        """
        scale = scale / self.q_levels
        zero = round_ste_fn.apply(zero)

        # quant
        x_quant = torch.clamp(round_ste_fn.apply(x / scale) + zero, 0, self.max_level)

        # dequant
        return (x_quant - zero) * scale

    def _repr_params(self):
        return f'num_bits={self.num_bits}'

    def __repr__(self):
        return f'{self.__class__.__name__}(self._repr_params())'


class FakeQuantization(nn.Module):
    """Abstract base class for fake quantization modules
    """
    def __init__(self):
        super(FakeQuantization, self).__init__()

        self._frozen = False

    @property
    def frozen(self):
        """If True, the scale_wt should be frozen and not updated anymore
        """
        return self._frozen

    @classmethod
    def set_frozen_state(cls, model, state):
        """Freeze quantizer parameters in a model
        """
        for module in model.modules():
            if isinstance(module, cls):
                module.frozen = state


class FakeLinearConstQuantization(FakeQuantization):
    """Fake linear symmetric quantization module with constant scale factor
    """
    def __init__(self, scale=1.0, **kwargs):
        """
        scale (float):          The constant scale factor.
        """
        super(FakeLinearConstQuantization, self).__init__()

        self.quant_core = FakeLinearSymmetricQuantCore(**kwargs)
        self._scale = torch.Tensor([scale])
        self._frozen = True

    def forward(self, x):
        return self.quant_core(x, self._scale)

    @property
    def scale(self):
        return self._scale

    @FakeQuantization.frozen.setter
    def frozen(self, val):
        self._frozen = True     # always 'frozen' since its a constant

    def __repr__(self):
        return f'{self.__class__.__name__}({self.quant_core._repr_params()}, frozen={self._frozen}, scale={self._scale})'


def _format_scales(scales):
    """Format scales for printing to string
    """
    if scales is not None:
        used_scales = torch.unique(scales).cpu().numpy()
        used_scales_str = f'{used_scales}'
        if len(used_scales) > 10:
            start_str = f'{used_scales[:3]}'[:-1]
            end_str = f'{used_scales[-3:]}'[1:]
            used_scales_str = f'{start_str} ... {end_str}'
    else:
        used_scales_str = str(scales)

    return used_scales_str


class FakeLinearTrainedQuantization(FakeQuantization):
    """Fake linear quantization module with trainable quantizer parameters
    """
    def __init__(self, per_channel=False, use_pot_scale=False, use_zero_point=False, init_scale=1.0, init_zero=0.0,
                 ema_decay=0.9999, ema_decay_grad=0.98, **kwargs):
        """
        per_channel (bool):     If True, a separate scale factor (scale_wt) will be used for every channel
        use_pot_scale (bool):   If True, use power-of-two scale factor instead of floating point scale factor
        use_zero_point (bool):  If True, use asymmetric quantizer with zero-points, else use a symmetric quantizer
        init_scale (float):     Initial value of the scale factor. Non power-of-two values are
                                pushed towards power-of-two values during training unless no_pot is True
        init_zero (float):      Initial value  of the zero-point.
        ema_decay (float):      Decay for keeping track of EMA of scale_wt
        ema_decay_grad (float): Decay for keeping track of EMA of scale_wt gradient

        NOTE: Scale factor(s) are processed in log2 domain because of the following reasons:
        * Number stability: the scale cannot become zero
        * Easy to switch to/from power-of-two scales
        * More stable training results
        """
        super(FakeLinearTrainedQuantization, self).__init__()

        self.per_channel = per_channel
        self.init_scale = init_scale
        self.use_pot_scale = use_pot_scale
        self.use_zero_point = use_zero_point

        assert not (per_channel and use_zero_point), "Mixing per-channel with zero-points is currently not supported"

        if per_channel:
            # we don't yet know the number of channels the input will use, so we have to wait until the first forward pass
            # to be able to know the number of channels
            self.register_parameter('scale_wt', None)
        else:
            self.register_parameter('scale_wt', nn.Parameter(torch.Tensor([np.log2(init_scale)])))

        if use_zero_point:
            self.quant_core = FakeLinearAsymmetricQuantCore(**kwargs)
            self.register_parameter('zero_wt', nn.Parameter(torch.Tensor([init_zero])))
        else:
            self.quant_core = FakeLinearSymmetricQuantCore(**kwargs)

        self.register_buffer('_frozen_channels', None)
        if use_pot_scale:
            # These are used for determining when to freeze (stop training) the POT scale factor(s)
            self.ema_scale_wt = ExponentialMovingAverage(ema_decay)
            self.ema_scale_wt_grad = ExponentialMovingAverage(ema_decay_grad)

            self.scale_wt_grad_ema = None

            # register (in case of per_channel, do this in forward) backward hook to keep track of the latest (smoothed) gradient value of scale_wt
            def backward_hook(grad):
                self.scale_wt_grad_ema = self.ema_scale_wt_grad(grad)
                if self.per_channel:
                    # mask gradient to individually freeze tensor elements during training
                    grad_mask = match_dims(self._frozen_channels, grad, self.quant_core.per_channel_dim).expand_as(grad)
                    grad[grad_mask] = 0
                return grad

            self.hook_fn = backward_hook

            if not per_channel:
                self.hook = self.scale_wt.register_hook(self.hook_fn)

            self.round_fn = ceil_ste_fn.apply
        else:
            self.round_fn = nn.Identity()


    def forward(self, x):
        if self.scale_wt is None:
            # init per_channel scale parameters
            num_channels = x.size(self.quant_core.per_channel_dim)
            self.scale_wt = nn.Parameter(x.new(torch.Tensor(num_channels * [np.log2(self.init_scale)])))
            self._frozen_channels = x.new_zeros(num_channels).bool()
            if self.use_pot_scale:
                self.hook = self.scale_wt.register_hook(self.hook_fn)

        # Keep track of EMA of scale during training in case of POT scales
        if self.training and self.use_pot_scale:
            # Detach needed here to avoid 'memory leak' due to growing computational graph
            # https://discuss.pytorch.org/t/memory-leak-debugging-and-common-causes/67339
            self.scale_wt_ema = self.ema_scale_wt(self.scale_wt.detach())

        scale = torch.pow(2, self.round_fn(self.scale_wt))
        if self.use_zero_point:
            return self.quant_core(x, scale, self.zero_wt)

        return self.quant_core(x, scale)

    @FakeQuantization.frozen.setter
    def frozen(self, val):
        """Freeze/unfreeze all trainable scale values of this module
        """
        self._frozen = val
        if self.scale_wt is not None:
            self.scale_wt.requires_grad = not val

    @property
    def frozen_channels(self):
        if not self.per_channel:
            return self.scale_wt.new_tensor([self.frozen]).bool()

        return self._frozen_channels

    @frozen_channels.setter
    def frozen_channels(self, val):
        """Freeze/unfreeze separate elements in scale in case per_channel == True
        """
        if not self.per_channel:
            self.frozen = True if val else False
            return

        if not isinstance(val, torch.Tensor) or val.dtype != torch.bool:
            raise ValueError("frozen_channels property must be a tensor with dtype torch.bool")

        if len(val) != len(self.scale_wt):
            raise ValueError("Length of frozen ({}) must equal lenth of scale_wt ({})".format(len(val), len(self.scale_wt)))

        if self._frozen_channels is None:
            return

        self._frozen_channels = self._frozen_channels.new(val)

        if torch.all(self._frozen_channels):
            self.frozen = True
        if not torch.any(self._frozen_channels):
            self.frozen = False

    @property
    def scale(self):
        if self.scale_wt is None:
            return None
        return torch.pow(2, self.round_fn(self.scale_wt.detach()))

    def __repr__(self):
        zero_str = ""
        if self.use_zero_point:
            zero_str = f'zero_pt=[{round(self.zero_wt.item())}], '
        return f'{self.__class__.__name__}({self.quant_core._repr_params()}, frozen={self._frozen}, ' + \
               f'used_scales={_format_scales(self.scale)}, {zero_str}per_channel={self.per_channel})'

    @classmethod
    def freeze_stabilized_scales(cls, model, max_num_channels=400):
        """
        NOTE: automatic freezing is only applied when a modules 'use_pot_scale' attribute is True

        Freeze at most max_num_channels per channel scales spread over the whole network with the following conditions:
            * the scale_wt scale of that channel must be located in the most stable/popular power-of-2 bin (compared against EMA of scale_wt)
            * the smoothed scale_wt gradient magnitude of the candidate scales must be within the top 'max_num_channels' smallest within the network

        The method return a list with tuples containing (name, module ref, one_hot_channel_mask, |grad ema|) of the newly frozen scales,
        the number of frozen scales in total and the number of scales
        If a layer has only a single scale (per_channel == False) and its |grad ema| is the smallest among all other |grad ema| values, this scale
        will be frozen only and the algorithm will not consider freezing other channels or layers (if any).
        """
        # Iterate network and build a candidate list of channels that could be frozen (condition = if layer/channel is not frozen and stabilized)
        num_per_layer_scales = 0
        num_frozen_scales = 0
        num_scales = 0

        candidate_abs_grad_ema = []
        candidate_indices = []
        candidate_modules = []
        for name, module in model.named_modules():
            if not isinstance(module, cls):
                continue

            if module.use_pot_scale:
                # not frozen & stabilized condition
                candidate_mask = ~module.frozen_channels & (torch.ceil(module.scale_wt) == torch.ceil(module.scale_wt_ema))
            else:
                # no candidates if use_pot_scale == False: generate mask filled with 'False' elements
                candidate_mask = module.frozen_channels.new_zeros(len(module.frozen_channels)).bool()
            num_scales += len(candidate_mask)
            if not module.per_channel:
                num_per_layer_scales += 1
            num_frozen_scales += module.frozen_channels.sum()
            if torch.any(candidate_mask):
                candidate_abs_grad_ema.append(torch.abs(module.scale_wt_grad_ema[candidate_mask]))
                candidate_indices.append(candidate_mask.nonzero())
                candidate_modules.extend(len(candidate_mask.nonzero()) * [(name, module)])

        # stop early if no candidates where found
        if len(candidate_abs_grad_ema) == 0:
            return [], num_frozen_scales, num_scales

        candidate_abs_grad_ema = torch.cat(candidate_abs_grad_ema)
        candidate_indices = torch.cat(candidate_indices)

        # Select the top k candidates with smallest absolute gradient magnitude (sorted topk)
        k = min(max_num_channels + num_per_layer_scales, len(candidate_abs_grad_ema))
        _, topk_indices = candidate_abs_grad_ema.topk(k, largest=False, sorted=True)

        # Loop over the topk candidates and freeze them
        frozen_entries = []
        for index in topk_indices:
            name, module = candidate_modules[index]
            one_hot_channel_mask = candidate_indices.new_zeros(len(module.frozen_channels)).bool().scatter_(0, candidate_indices[index], True)
            entry = (name, module, one_hot_channel_mask, candidate_abs_grad_ema[index])

            if module.per_channel:
                module.frozen_channels |= one_hot_channel_mask
                frozen_entries.append(entry)
            # in case the top 1 candidate uses one scale for its layer, freeze only that layer and exit
            elif len(frozen_entries) == 0:
                module.frozen_channels |= one_hot_channel_mask
                frozen_entries.append(entry)
                break

            if len(frozen_entries) == max_num_channels:
                break

        return frozen_entries, num_frozen_scales + len(frozen_entries), num_scales


class FakeLinearEstimatedQuantization(FakeQuantization):
    """Fake linear quantization module with simple statistically estimated quantizer parameters.
    Most usefull for post-training quantization
    """
    def __init__(self, per_channel=False, use_pot_scale=False, init_scale=1.0,
                 estimator='3std', ema_decay=0.999, **kwargs):
        """
        per_channel (bool):     If True, a separate scale factor  will be used for every channel
        use_pot_scale (bool):   If True, use power-of-two scale factor instead of floating point scale factor
        init_scale (float):     Initial value of the scale factor. Non power-of-two values are
                                pushed towards power-of-two values during training unless no_pot is True
        estimator (str):        Estimator algorithm that is used to estimate the scale based
                                on the tensor data.
        ema_decay (float):      Decay value for EMA of the scale. The final estimation of the scale is this EMA in case
                                multiple batches of data are forwarded.

        NOTE: Scale factor(s) are stored in log2 domain. This way they are easily transferrable to FakeLinearTrainedQuantization
        which uses log2.
        """
        super(FakeLinearEstimatedQuantization, self).__init__()

        self.per_channel = per_channel
        self.use_pot_scale = use_pot_scale
        self.init_scale = init_scale
        self.estimator = estimator
        self.ema_scale = ExponentialMovingAverage(ema_decay)

        if per_channel:
            # we don't yet know the number of channels the input will use, so we have to wait until the first forward pass
            # to be able to know the number of channels
            self.register_buffer('scale_wt', None)
        else:
            self.register_buffer('scale_wt', torch.Tensor([np.log2(init_scale)]))

        self.quant_core = FakeLinearSymmetricQuantCore(**kwargs)

        if use_pot_scale:
            self.round_fn = torch.ceil
        else:
            self.round_fn = nn.Identity()

        self.estimator_fn = self._get_estimator_fn(estimator)

    def forward(self, x):
        if not self._frozen and self.training:
            with torch.no_grad():
                scale = self.estimator_fn(x)
                scale = torch.clamp(scale, min=1e-9)    # ensure scale is never zero
                scale_wt = torch.log2(scale)            # convert to log2 domain
                self.scale_wt = self.ema_scale(scale_wt)
        elif self.scale_wt is None:
            # if per channel is True and _frozen is True, we need an init
            self.scale_wt = x.new(torch.Tensor(x.size(self.quant_core.per_channel_dim) * [np.log2(self.init_scale)]))

        #self.last_x = x
        return self.quant_core(x, self.scale)

    @property
    def scale(self):
        if self.scale_wt is None:
            return None
        return torch.pow(2, self.round_fn(self.scale_wt))

    def __repr__(self):
        return f'{self.__class__.__name__}({self.quant_core._repr_params()}, frozen={self._frozen}, ' + \
               f'used_scales={_format_scales(self.scale)}, per_channel={self.per_channel}, estimator={self.estimator})'

    def _get_estimator_fn(self, name):
        """Return a scale estimator callable based on a given name
        Raise an error if estimator name is not recognized
        """
        if name == 'max':
            estimator_fn = self.estimate_max
        elif name == '3std':
            estimator_fn = self.estimate_3std
        elif name == 'mse':
            estimator_fn = self.estimate_mse
        elif name == 'klj':
            estimator_fn = self.estimate_klj

        if self.per_channel:
            # apply estimator function for every channel seperatly
            wrapped_estimator_fn = lambda x: torch.stack([estimator_fn(x_c) for x_c in x.transpose(self.quant_core.per_channel_dim, 0)])
        else:
            wrapped_estimator_fn = lambda x: estimator_fn(x).unsqueeze(0)

        return wrapped_estimator_fn

    @FakeQuantization.frozen.setter
    def frozen(self, val):
        self._frozen = val

    def estimate_max(self, x):
        """Estimate based on max
        """
        return torch.max(torch.abs(x))

    def estimate_3std(self, x):
        """Estimate based on 3 times the standard deviation
        """
        return (torch.abs(torch.mean(x)) + 3 * torch.std(x))

    def estimate_mse(self, x):
        """Estimate based on mean-square-error
        """
        scale = self.estimate_max(x)

        if self.use_pot_scale:
            # use grid search, start with highest POT-scale, divide up to 5 times and calculate L2 norm
            # use scale with lowest squared Frobenius norm
            pot_scale = torch.pow(2, torch.ceil(torch.log2(scale)))

            best_error = 10000000
            scale = pot_scale
            for i in range(5):
                q = self.quant_core(x, pot_scale)
                error = torch.norm(q.flatten() - x.flatten())**2
                if error < best_error:
                    best_error = error
                    scale = pot_scale
                pot_scale = pot_scale / 2

        else:
            # estimate scale by minimizing the squared Frobenius norm between x and q
            for i in range(100):
                q = self.quant_core(x, scale)
                q0 = q.detach() / scale
                scale = torch.sum(x.detach() * q0) / torch.sum(q0 * q0)

        return scale

    def estimate_klj(self, data):
        """Numpy algorithm from decompiled (uncompile6) graffitist code:
        https://github.com/Xilinx/graffitist/blob/master/graffitist/utils/quantize_utils.pyc

        Originaly proposed in:
        https://arxiv.org/pdf/1805.07941.pdf
        """
        n = int(self.quant_core.q_levels_per_sign)

        data = torch.abs(data.reshape(-1))
        bins = math.ceil(math.sqrt(data.size(0)))
        hist = torch.histc(data, bins)
        pdf = hist / torch.sum(hist)
        cdf = torch.cumsum(pdf, dim=0)

        # Keep it simple in case all histogram bins can already be represented by the
        # number of bits, otherwise do distribution comparison
        if n + 1 > hist.size(0):
            return data.max()

        def measure_klj(x, y):
            """Kullback-Leibler-J divergence
            ref: Section 4.1 in "D’Alberto, P. and Dasdan, A., Non-Parametric Information-Theoretic
                 Measures of One-DimensionalDistribution Functions from Continuous Time Series, 2009"
            """
            return torch.sum((x - y) * torch.log2(x / y))

        thresholds = []
        measures = []
        step = (data.max() - data.min()) / bins

        for i in range(n + 1, hist.size(0) + 1):
            threshold_tmp = (i + 0.5) * step
            thresholds.append(threshold_tmp)
            p = cdf.clone()
            p[i - 1:] = 1
            fp = p[:i].unsqueeze(0).unsqueeze(0)  # needs to be 3D for interpolate
            p_interp = torch.nn.functional.interpolate(fp, size=(n), align_corners=True, mode='linear')
            q_interp = torch.nn.functional.interpolate(p_interp, size=(i), align_corners=True, mode='linear')
            q = p.clone()
            q[:i] = q_interp[0][0]
            measure = measure_klj(cdf[torch.nonzero(cdf)], q[torch.nonzero(cdf)])
            measures.append(measure)

        thresholds = torch.stack(thresholds)
        measures = torch.stack(measures)
        threshold_final = thresholds[torch.argmin(measures)]

        return threshold_final


class FakeLinearAsymmetricEstimatedQuantization(FakeQuantization):
    """Fake linear quantization module with simple statistically estimated quantizer parameters.
    Most usefull for post-training quantization
    """
    def __init__(self, use_pot_scale=False, init_scale=1.0, init_zero=0,
                 estimator='minmax', ema_decay=0.999, **kwargs):
        """
        use_pot_scale (bool):   If True, use power-of-two scale factor instead of floating point scale factor
        init_scale (float):     Initial value of the scale factor. Non power-of-two values are
                                pushed towards power-of-two values during training unless no_pot is True
        init_zero (int):        Initial value  of the zero-point.
        estimator (str):        Estimator algorithm that is used to estimate the scale and zero point based
                                on the tensor data.
        ema_decay (float):      Decay value for EMA of the scale and zero. The final estimation of the scale is this EMA in case
                                multiple batches of data are forwarded.

        NOTE: Scale factor(s) are stored in log2 domain. This way they are easily transferrable to FakeLinearTrainedQuantization
        which uses log2.
        """
        super(FakeLinearAsymmetricEstimatedQuantization, self).__init__()

        self.use_pot_scale = use_pot_scale
        self.estimator = estimator

        # usefull info for MSE
        self.dual_tailored_dist = True
        if 'is_signed' in kwargs and not kwargs['is_signed']:
            self.dual_tailored_dist = False

        self.quant_core = FakeLinearAsymmetricQuantCore(**kwargs)
        self.register_buffer('scale_wt', torch.Tensor([np.log2(init_scale)]))
        self.register_buffer('zero_wt', torch.Tensor([init_zero]))
        self.ema_scale = ExponentialMovingAverage(ema_decay)
        self.ema_zero = ExponentialMovingAverage(ema_decay)

        if use_pot_scale:
            self.round_fn = torch.ceil
        else:
            self.round_fn = nn.Identity()

        self.estimator_fn = self._get_estimator_fn(estimator)

    def forward(self, x):
        if not self._frozen and self.training:
            with torch.no_grad():
                scale, zero = self.estimator_fn(x)
                self.zero_wt = self.ema_zero(zero)
                scale = torch.clamp(scale, min=1e-9)    # ensure scale is never zero
                scale_wt = torch.log2(scale)            # convert to log2 domain
                self.scale_wt = self.ema_scale(scale_wt)

        #self.last_x = x
        return self.quant_core(x, self.scale, self.zero_wt)

    # TODO: make common
    @property
    def scale(self):
        if self.scale_wt is None:
            return None
        return torch.pow(2, self.round_fn(self.scale_wt))

    def __repr__(self):
        return f'{self.__class__.__name__}({self.quant_core._repr_params()}, frozen={self._frozen}, ' + \
               f'used_scale={_format_scales(self.scale)}, zero_pt=[{round(self.zero_wt.item())}], estimator={self.estimator})'

    def _get_estimator_fn(self, name):
        """Return a scale estimator callable based on a given name
        Raise an error if estimator name is not recognized
        """
        if name == 'minmax':
            estimator_fn = self.estimate_min_max
        elif name == 'mse':
            estimator_fn = self.estimate_mse
        else:
            raise ValueError(f"Estimator {name} unknown")

        return estimator_fn

    # TODO: make common
    @FakeQuantization.frozen.setter
    def frozen(self, val):
        self._frozen = val

    def estimate_min_max(self, x):
        """Estimate based on min-max metrics
        """
        maximum = torch.max(x)
        minimum = torch.min(x)
        scale = (maximum - minimum)
        zero = torch.round(- minimum / scale * (self.quant_core.max_level))
        return scale.unsqueeze(0), zero.unsqueeze(0)

    def estimate_mse(self, x):
        """Use mean-square-error to estimate optimal scale factor and zero point
        """
        x = x.detach()
        scale, zero = self.estimate_min_max(x)

        if self.use_pot_scale:
            # use grid search, start with highest POT-scale, divide up to 5 times and calculate L2 norm
            # use scale with lowest L2 norm
            pot_scale = torch.pow(2, torch.ceil(torch.log2(scale)))

            best_error = 10000000
            scale = pot_scale
            for i in range(5):
                q = self.quant_core(x, pot_scale, zero)
                error = torch.norm(q.flatten() - x.flatten(), 2)
                if error < best_error:
                    best_error = error
                    scale = pot_scale
                pot_scale = pot_scale / 2
        else:
            if self.dual_tailored_dist:
                # shift x to center to make it a symmetric distribution
                x_center = x - x.min() - scale / 2

                # optimize scale symmetrically
                new_scale = scale
                zero_center = new_scale.new_full((1,), self.quant_core.q_levels/2)
                for i in range(100):
                    q = self.quant_core(x_center, new_scale, zero_center)
                    q0 = q / new_scale
                    new_scale = torch.sum(x_center * q0) / torch.sum(q0 * q0).unsqueeze(0)

                # update zero to new scale
                zero = self.quant_core.q_levels/new_scale * (new_scale - scale) / 2 + zero * scale / new_scale
                scale = new_scale
            else:
                for i in range(100):
                    q = self.quant_core(x, scale, zero)
                    q0 = q / scale
                    scale = torch.sum(x * q0) / torch.sum(q0 * q0).unsqueeze(0)

        return scale, zero


class FakeLinearPPQuantization(FakeQuantization):
    """Fake linear quantization module with estimated floating point scale factor using the PPQ algorithm
    (https://arxiv.org/pdf/1903.01061.pdf). This algorithm works similar as QEM but uses a linear quantization approch
    """
    def __init__(self, init_scale=1.0, ema_decay=0.99, iterations=1, **kwargs):
        """
        quant_core (nn.Module):     Module that takes input data and parameters and produces the quantized output
        init_scale (float):         Initial value of the scale factor.
        ema_decay (float):          Decay value for EMA of the scale. The final value is the EMA rather that the direct estimate
        iterations (int):           How many iterations PPQ is doing for each forward pass
        """
        super(FakeLinearPPQuantization, self).__init__()

        self.quant_core = FakeLinearSymmetricQuantCore(**kwargs)
        self.register_buffer('scale_wt', torch.Tensor([init_scale]))
        self.iterations = iterations
        self.ema = ExponentialMovingAverage(ema_decay)

    def _forward(self, x):
        eps = 1e-6
        scale = self.scale_wt
        for i in range(self.iterations):
            q = self.quant_core(x, scale)
            q0 = q.detach() / scale
            scale = torch.clamp(torch.sum(x.detach() * q0) / (torch.sum(q0 * q0) + eps), min=eps)

        self.scale_wt = self.ema(scale)

        return q

    def forward(self, x):
        q = x
        if not self._frozen and self.training:
            q = self._forward(x)
        else:
            q = self.quant_core(x, self.scale_wt)

        return q

    @FakeQuantization.frozen.setter
    def frozen(self, val):
        self._frozen = val

    @property
    def scale(self):
        return self.scale_wt

    def __repr__(self):
        return f'{self.__class__.__name__}({self.quant_core._repr_params()}, frozen={self._frozen}, used_scales={_format_scales(self.scale)})'

    @classmethod
    def freeze_stabilized_scales(cls, model):
        """For now, just freeze all layers at once
        """

        frozen_scales = []
        num_frozen_scales = 0
        num_scales = 0
        for name, module in model.named_modules():
            if isinstance(module, cls):
                num_scales += 1
                if not module.frozen:
                    module.frozen = True
                    frozen_scales.append(name)

                if module.frozen:
                    num_frozen_scales += 1

        return frozen_scales, num_frozen_scales, num_scales


#class FakePOTPPQuantization(FakeQuantization):
#    """Fake symmetric quantization module with estimated power-of-two point scale factor using power-of-two variant
#    of the PPQ algorithm (our own)
#    """
#    def __init__(self, quant_core, init_scale=1.0, ema_scale_decay=0.9999, ema_error_decay=0.99, iterations=1, enable_ab=False, **kwargs):
#        """
#        quant_core (nn.Module):     Module that takes input data and parameters and produces the quantized output
#        init_scale (float):         Initial value of the scale factor. Will be rounded to POT scale internally
#        ema_decay (float):          Decay value for EMA of the scale. Used for the stabilization criteria
#        iterations (int):           How many iterations PPQ is doing for each forward pass
#        enable_ab (bool):           Enable alpha blending
#        """
#        super(FakePOTPPQuantization, self).__init__(quant_core, enable_ab=enable_ab)
#
#        # ensure init is power-of-two
#        pot_init_scale = np.power(2, np.round(np.log2(init_scale)))
#        self.register_buffer('scale_wt', torch.Tensor([pot_init_scale]))
#        self.scale_wt_ema = None
#        self.scale_error = None
#        self.iterations = iterations
#        self.ema_scale = ExponentialMovingAverage(ema_scale_decay)
#        self.ema_error = ExponentialMovingAverage(ema_error_decay)
#
#    def _quant(self, x, scale):
#        q = self.quant_core(x, scale)
#        q0 = q.detach() / scale
#        scale_error = torch.sum(x.detach() * q0) / torch.sum(q0 * q0) - scale
#        return q, scale_error
#
#    def _forward(self, x):
#        q = x
#        for i in range(self.iterations):
#            q, scale_error = self._quant(x, self.scale_wt)
#            new_scale = self.scale_wt / 2.0 if scale_error < 0 else self.scale_wt * 2.0
#            new_q, new_scale_error = self._quant(x, new_scale)
#
#            # use quant output of newly estimated scale if its error is smaller
#            best_scale_error = scale_error
#            best_scale = self.scale_wt
#            if torch.abs(new_scale_error) < torch.abs(scale_error):
#                best_scale_error = new_scale_error
#                best_scale = new_scale
#
#            self.scale_error = self.ema_error(best_scale_error)
#            self.scale_wt_ema = self.ema_scale(best_scale)
#
#            if new_scale == torch.pow(2, torch.round(torch.log2(self.scale_wt_ema))):
#                q = new_q
#                self.scale_wt = new_scale
#
#        return q
#
#    def forward(self, x):
#        q = x
#        if not self._frozen and self.training:
#            q = self._forward(x)
#        else:
#            q = self.quant_core(x, self.scale_wt)
#
#        return self.blend(q, x)
#
#    @FakeQuantization.frozen.setter
#    def frozen(self, val):
#        self._frozen = val
#
#    @property
#    def scale(self):
#        return self.scale_wt
#
#    def __repr__(self):
#        return self._repr(self.scale_wt)
#
#    @classmethod
#    def freeze_stabilized_scales(cls, model):
#        """Freeze criteria: freeze the scale with the smallest module.scale_error if and only if
#        the scale is within the most popular power-of-two bin (module.scale == pot_quant(module.scale_wt_ema))
#        and if that scale is not yet frozen.
#        """
#        num_frozen_scales = 0
#        num_scales = 0
#        candidate_modules = []
#        candidate_scale_errors = []
#
#        for name, module in model.named_modules():
#            if not isinstance(module, cls):
#                continue
#
#            num_scales += 1
#            if module.frozen:
#                num_frozen_scales += 1
#            else:
#                candidate_modules.append((name, module))
#                candidate_scale_errors.append(module.scale_error)
#
#        if len(candidate_modules) == 0:
#            return [], num_frozen_scales, num_scales
#
#        candidate_scale_errors = torch.cat(candidate_scale_errors)
#        _, topk_indices = candidate_scale_errors.topk(1, largest=False, sorted=True)
#
#        frozen_scales = []
#        for index in topk_indices:
#            name, module = candidate_modules[index]
#            frozen_scales.append(name)
#            num_frozen_scales += 1
#            module.frozen = True
#
#        return frozen_scales, num_frozen_scales, num_scales


class FakeNonLinearQEMQuantization(FakeQuantization):
    """Fake non linear quantizer that estimates all quantization levels individually using the QEM algorithm
    (https://arxiv.org/abs/1807.10029). This algorithm has a similar effect as K-means.
    """

    def __init__(self, num_bits=4, num_bits_q_levels=8, q_level_ema_decay=0.9, q_level_stab_ema_decay=0.9999, **kwargs):
        """
        num_bits (int):                     Used to determine the number of levels which is 2^num_bits
        num_bits_q_levels (int):            Number of bits to represent the q levels.
                                            by a full floating point number
        q_level_ema_decay (float):          EMA decay for estimated quantization levels
        q_level_stab_ema_decay (float):     EMA decay for estimated quantization levels
        """
        super(FakeNonLinearQEMQuantization, self).__init__()

        self.num_bins = 2**(num_bits_q_levels - 1)
        self.num_bits = num_bits
        self.min_level = - self.num_bins
        self.max_level = self.num_bins - 1
        self.register_buffer('q_levels', torch.linspace(self.min_level, self.max_level, 2**num_bits))
        self.register_buffer('scale_wt', torch.log2(torch.Tensor([1.0])))
        self.q_levels_stab = None
        self.ema = ExponentialMovingAverage(decay=q_level_ema_decay)
        self.ema_stab = ExponentialMovingAverage(decay=q_level_stab_ema_decay)

    def forward(self, x):
        """Apply the quantization
        """
        # calculate POT scale based on log2 scale
        scale = self.scale / self.num_bins
        scaled_x = x / scale
        q, idxs = proj_ste_fn.apply(scaled_x, self.q_levels)

        if not self._frozen and self.training:
            with torch.no_grad():
                # implement optimization of q levels through indexing instead of multiplication with
                # binary matrix B to save memory and computational resources.
                new_q_levels = torch.empty_like(self.q_levels)
                flattend_scaled_x = scaled_x.flatten()
                for i in range(len(self.q_levels)):
                    values = flattend_scaled_x[idxs == i]
                    if len(values) != 0:
                        new_q_levels[i] = values.mean()
                    else:
                        # If no data is present for this level, reset it to zero to ensure
                        # it will be optimized (assuming that data is present close to zero).
                        # NOTE: q levels can switch places because of this reset
                        new_q_levels[i] = 0.0

                # original equivalent which is shorter but uses a lot more memory and more computations
                #B = torch.nn.functional.one_hot(idxs.reshape(-1), len(self.q_levels)).float().transpose(0, 1)
                # eps to avoid division by zero
                #eps = 1e-6
                #new_q_levels = torch.mv(B, scaled_x.reshape(-1)) / (torch.sum(B, dim=1) + eps)

                # limit range of newly estimated q levels
                new_q_levels = torch.clamp(new_q_levels, self.min_level, self.max_level)

                # update q levels
                self.q_levels = self.ema(new_q_levels)
                self.q_levels_stab = self.ema_stab(self.q_levels)

        return q * scale

    @FakeQuantization.frozen.setter
    def frozen(self, val):
        self._frozen = val
        if val:
            # ensure q_levels are rounded
            self.q_levels = torch.round(self.q_levels)

    @property
    def scale(self):
        return torch.pow(2, torch.ceil(self.scale_wt))

    def __repr__(self):
        q_levels = _format_scales(torch.sort(self.q_levels)[0])
        return f'{self.__class__.__name__}(frozen={self._frozen}, scale={self.scale.item()}, q_levels={q_levels})'

    @classmethod
    def pre_run(cls, model, device, num_iterations=500, progress_bar=True):
        """Run random data through the model in training mode without backprop and without weight update.
        This will enable QEM to stabilize to the initial weight distributions.
        the q levels.
        """
        # forward module with random data
        dummy_data = torch.randn(*model.input_shape)
        dummy_data = dummy_data.to(device)
        for i in tqdm(range(num_iterations)) if progress_bar else range(num_iterations):
            with torch.no_grad():
                model(dummy_data)

        # reset stabilization ema
        for module in model.modules():
            if isinstance(module, cls):
                module.ema_stab.biased_ema = 0
                module.ema_stab.step = 0

    @classmethod
    def freeze_stabilized_q_levels(cls, model):
        """Freeze and quantize the q levels from a single quantizer. Criteria:
        * q levels must be stabilized
        * the q levels error must be the smallest within the whole model

        The stabilization criteria:
        * round q_levels
        * round q_levels_stab
        * if both rounded q_levels and rounded q_levels_stab are equal, the criteria is met
        """

        num_quantizers = 0
        num_frozen_quantizers = 0
        candidate_modules = []
        candidate_q_level_errors = []

        for name, module in model.named_modules():
            if not isinstance(module, cls):
                continue

            num_quantizers += 1
            if module.frozen:
                num_frozen_quantizers += 1
                continue

            # check stabilization criteria
            if (torch.round(module.q_levels) == torch.round(module.q_levels_stab)).all():
                candidate_modules.append((name, module))
                # calculate q level error
                error = torch.norm(module.q_levels - torch.round(module.q_levels), 2)
                candidate_q_level_errors.append(error)

        if len(candidate_modules) == 0:
            return [], num_frozen_quantizers, num_quantizers

        candidate_q_level_errors = torch.stack(candidate_q_level_errors)
        _, topk_indices = candidate_q_level_errors.topk(1, largest=False, sorted=True)

        frozen_quantizers = []
        for index in topk_indices:
            name, module = candidate_modules[index]
            frozen_quantizers.append(name)
            num_frozen_quantizers += 1
            module.frozen = True  # the property setter will ensure the q_levels are rounded

        return frozen_quantizers, num_frozen_quantizers, num_quantizers

    @classmethod
    def freeze_stabilized_q_levels_simple(cls, model):
        """Freeze and quantize the q levels from a single quantizer.
        Selection criteria of layer to freeze:
        * Layer was not yet frozen
        * MSE between q_levels and q_levels_stab is smallest for that layer
        """

        num_quantizers = 0
        num_frozen_quantizers = 0
        candidate_modules = []
        candidate_mse_values = []

        for name, module in model.named_modules():
            if not isinstance(module, cls):
                continue

            num_quantizers += 1
            if module.frozen:
                num_frozen_quantizers += 1
                continue

            # calculate MSE between q_levels and q_levels_stab
            mse = torch.norm(module.q_levels - module.q_levels_stab)**2
            candidate_mse_values.append(mse)
            candidate_modules.append((name, module))

        print(candidate_mse_values)
        # in case all q levels are already frozen
        if len(candidate_modules) == 0:
            return [], num_frozen_quantizers, num_quantizers

        candidate_mse_values = torch.stack(candidate_mse_values)
        _, topk_indices = candidate_mse_values.topk(1, largest=False, sorted=True)

        frozen_quantizers = []
        for index in topk_indices:
            name, module = candidate_modules[index]
            frozen_quantizers.append(name)
            num_frozen_quantizers += 1
            module.frozen = True  # the property setter will ensure the q_levels are rounded

        return frozen_quantizers, num_frozen_quantizers, num_quantizers


class FakeNonLinearEstimatedQuantization(FakeQuantization):
    """Fake non linear quantizer that estimates all quantization levels individually using the QEM algorithm
    (https://arxiv.org/abs/1807.10029). This algorithm has a similar effect as K-means.
    """

    def __init__(self, num_bits=4, num_bits_q_levels=8, iterations=100, **kwargs):
        """
        num_bits (int):                     Used to determine the number of levels which is 2^num_bits
        num_bits_q_levels (int):            Number of bits to represent the q levels.
                                            by a full floating point number
        q_level_ema_decay (float):          EMA decay for estimated quantization levels
        q_level_stab_ema_decay (float):     EMA decay for estimated quantization levels
        """
        super(FakeNonLinearEstimatedQuantization, self).__init__()

        self.iterations = iterations
        self.quant_core = FakeNonLinearQEMQuantization(num_bits, num_bits_q_levels, 0.0)
        self.register_buffer('q_levels', self.quant_core.q_levels)
        self.register_buffer('scale_wt', self.quant_core.scale_wt)

    def forward(self, x):
        if not self._frozen and self.training:
            with torch.no_grad():
                scale = torch.max(torch.abs(x))
                scale = torch.ceil(torch.log2(scale)).unsqueeze(0)

                best_error = 10000000
                q_levels_init = self.quant_core.q_levels.clone()
                for i in range(5):
                    self.quant_core.frozen = False
                    self.quant_core.scale_wt = scale.clone()
                    self.quant_core.q_levels = q_levels_init.clone()
                    for j in range(self.iterations):
                        q = self.quant_core(x)
                    error = torch.norm(q.flatten() - x.flatten())**2
                    if error < best_error:
                        best_error = error
                        self.scale_wt = scale.clone()
                        self.q_levels = self.quant_core.q_levels.clone()
                    scale -= 1
                self.quant_core.q_levels = self.q_levels
                self.quant_core.scale_wt = self.scale_wt

        return self.quant_core(x)

    def __repr__(self):
        q_levels = _format_scales(torch.sort(self.q_levels)[0])
        return f'{self.__class__.__name__}(frozen={self._frozen}, scale={self.scale.item()}, q_levels={q_levels})'

    @property
    def scale(self):
        return torch.pow(2, torch.ceil(self.scale_wt))


class AIWQ(nn.Module):
    """Module that calculates and stores the Activation Instability by Weight Quantization metric
    paper: https://arxiv.org/pdf/1810.05723.pdf
    """

    def __init__(self, param_module_fn, ema_decay=0.999):
        super(AIWQ, self).__init__()

        self.param_module_fn = param_module_fn
        self.ema = ExponentialMovingAverage(ema_decay)
        self.register_buffer('metric', torch.tensor(0.0))
        self.prev_weight = None

    def forward(self, input, output):
        """input: input data of the convolution/linear operation
           output: quantized output data calculated by the convolution/linear with quantized weights
        """
        if self.prev_weight is None:
            return

        with torch.no_grad():
            num_channels = output.size(1)
            prev_output = self.param_module_fn(input, self.prev_weight)
            prev_output_t = prev_output.transpose(1, 0).reshape(num_channels, -1)
            prev_means = prev_output_t.mean(1)
            prev_stds = prev_output_t.std(1).clamp(min=1e-8)    # ensure > 0

            output_t = output.transpose(1, 0).reshape(num_channels, -1)
            means = output_t.mean(1)
            stds = output_t.std(1).clamp(min=1e-8)  # ensure > 0

            kl = torch.log(prev_stds / stds) + \
                 (stds ** 2  + (means - prev_means) ** 2) / (2 * prev_stds ** 2) - 0.5

            self.metric = self.ema(kl.mean())

    def __repr__(self):
        return '{}(metric={:.04})'.format(self.__class__.__name__, self.metric.item())
