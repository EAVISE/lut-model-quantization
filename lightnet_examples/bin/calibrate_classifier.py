import torch
import logging
import argparse
import distiller
import lightnet as ln

from dataset import ImagenetValidData
from utils import save_checkpoint, load_checkpoint, test_classifier


if __name__ == '__main__':
    log = logging.getLogger('lightnet.Imagenet.calibrate')

    parser = argparse.ArgumentParser(description='Calibrate classifier network')
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-o', '--output', help='Path to store calibrated weights', required=True)
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=16)
    parser.add_argument('-d', '--deterministic', action='store_true', help='Enable deterministic behavior')
    parser.add_argument('-t', '--testprior', action='store_true', help='Test prior to calibration')
    args = parser.parse_args()

    # Parse arguments
    if args.deterministic:
        log.info('Deterministic enabled')
        distiller.set_deterministic()

    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    params = ln.engine.HyperParameters.from_file(args.network)

    # Load weights
    if args.weight is not None:
        load_checkpoint(params.network, args.weight, False)

    # Create dataloaders for test and calibration sets
    valid_dataset = ImagenetValidData(params)
    valid_loader = torch.utils.data.DataLoader(
        valid_dataset,
        batch_size = params.batch_size_test,
        shuffle = False,
        drop_last = False,
        num_workers = args.workers,
        pin_memory = True,
    )

    calib_loader = torch.utils.data.DataLoader(
        torch.utils.data.Subset(valid_dataset, torch.randint(len(valid_dataset), (params.calib_size,))),
        batch_size = params.batch_size_calib,
        shuffle = False,
        drop_last = False,
        num_workers = args.workers,
        pin_memory = True,
    )

    if args.testprior:
        params.to(device)
        # Test network prior to calibration
        log.info('Starting test prior to calibration')
        tp1, tp5, loss = test_classifier(params.network, valid_loader, device, params.loss)
        log.info(f'Loss:{loss:.5f} TOP1:{tp1:.2f}% TOP5:{tp5:.2f}%')
        params.to('cpu')

    # Quantize network
    log.info('Quantizing network')
    if hasattr(params, 'quantizer_cls'):
        quantizer = params.quantizer_cls(params.network)
    else:
        # for backward compatibility with old configs: TODO: remove in future
        quantizer = distiller.quantization.EasicsQuantizer(params.network,
                                                           bits_activations=params.bits_activations,
                                                           bits_weights=params.bits_weights,
                                                           bits_bias=params.bits_bias,
                                                           quantize_inputs=params.quantize_inputs,
                                                           bits_inputs=params.bits_inputs,
                                                           signed_inputs=params.signed_inputs,
                                                           weight_quant_layer_cls=params.weight_quant_layer_cls,
                                                           act_quant_layer_cls=params.act_quant_layer_cls)
    quantizer.prepare_model()
    params.to(device)

    # Start calibration
    log.info('Starting calibration of quantizer parameters')

    # enable training mode during calibration, but freeze BN
    params.network.train()
    distiller.quantization.set_frozen_state_batchnorm_stats(params.network, True)
    test_classifier(params.network, calib_loader, device, progress_bar=False, set_eval=False)

    print(repr(params.network))

    # Test calibrated network
    log.info('Starting test on calibrated network')
    tp1, tp5, loss = test_classifier(params.network, valid_loader, device, params.loss)
    log.info(f'Loss:{loss:.5f} TOP1:{tp1:.2f}% TOP5:{tp5:.2f}%')

    # Save calibrated weights
    log.info(f'Saving weights to {args.output}')
    save_checkpoint(params, args.output, tp1=tp1, tp5=tp5, loss=loss)
