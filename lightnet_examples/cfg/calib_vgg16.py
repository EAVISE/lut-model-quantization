import lightnet as ln
import torch
from distiller.utils import set_model_input_shape_attr
from distiller.quantization import FakeLinearEstimatedQuantization
from functools import partial

import lightnet_examples.models

__all__ = ['params']

ROOT = 'data/imagenet/'
params = ln.engine.HyperParameters(
    # Network
    input_dimension = (224, 224),
    batch_size_test = 32,
    batch_size_calib = 50,

    calib_size = 50,

    # Dataset
    _valid_set = ROOT + 'val',

    # Preprocessing
    preprocessor_test = 'default_noaug',

    # Quantization
    quantize_inputs = True,
    bits_inputs = 8,
    signed_inputs = True,
    bits_activations = 8,
    bits_weights = 8,
    bits_bias = None,
    weight_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='3std', use_pot_scale=True),
    act_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='klj', use_pot_scale=True),
)

# Network
params.network = lightnet_examples.models.vgg16(pretrained=True)
set_model_input_shape_attr(params.network, input_shape=(1, 3, *params.input_dimension))

# Loss
params.loss = torch.nn.CrossEntropyLoss()
