import logging
from statistics import mean
from math import isinf, isnan
import lightnet as ln
from train import QuantizationAwareTrainingEngine, main

from dataset import COCOTrainData, COCOValidData
from utils import save_checkpoint, test_detector

log = logging.getLogger('lightnet.MSCOCO.train')


class TrainEngine(QuantizationAwareTrainingEngine):

    def task_start(self):
        self.train_loss = {}
        self.best_acc = 0

    def process_batch(self, data):
        data, target = data
        data = data.to(self.device)

        output = self.network(data)
        loss = self.loss(output, target) / self.batch_subdivisions
        loss.backward()

        for lname in sorted(l for l in dir(self.loss) if l.startswith('loss')):
            if not lname in self.train_loss:
                self.train_loss[lname] = []
            self.train_loss[lname].append(getattr(self.loss, lname).item())

    def task_train_batch(self):
        # Get values from last batch
        loss_values = ''
        for name, values in self.train_loss.items():
            value = mean(values[-self.batch_subdivisions:])
            loss_values += f'{name}:{value:.3f} '
        self.log(f'{self.batch} | {loss_values}')

        if isinf(self.loss.loss) or isnan(self.loss.loss):
            log.error('Infinite loss')
            self.sigint = True

    def task_plot_scalars(self):
        if len(self.train_loss) == 0:
            return

        tot = mean(self.train_loss['loss'])
        coord = mean(self.train_loss['loss_coord'])
        conf = mean(self.train_loss['loss_conf'])
        cls = mean(self.train_loss['loss_cls'])
        self.train_loss = {'loss': [], 'loss_coord': [], 'loss_conf': [], 'loss_cls': []}

        self.writer.add_scalar('Train/Loss_tot', tot, self.batch)
        self.writer.add_scalar('Train/Loss_coord', coord, self.batch)
        self.writer.add_scalar('Train/Loss_conf', conf, self.batch)
        self.writer.add_scalar('Train/Loss_cls', cls, self.batch)

    def test(self):
        self.log('Start testing')
        map_50, map_75, map_coco, loss = test_detector(self.network, self.valid_loader, self.device, self.params.post, self.params.loss, self)
        self.sigint = False

        self.network.train()
        self.loss.train()

        if map_50 is None:
            return

        self.log(f'loss_tot: {loss[0]:.2f}, loss_coord: {loss[1]:.2f}, loss_conf: {loss[2]:.2f}, loss_cls: {loss[3]:.2f}')
        self.log(f'mAP[0.50]:            {map_50:.2f}%')
        self.log(f'mAP[0.75]:            {map_75:.2f}%')
        self.log(f'mAP[0.50:0.95:0.05]:  {map_coco:.2f}%')

        self.writer.add_scalar('Test/Loss_tot', loss[0], self.batch)
        self.writer.add_scalar('Test/Loss_coord', loss[1], self.batch)
        self.writer.add_scalar('Test/Loss_conf', loss[2], self.batch)
        self.writer.add_scalar('Test/Loss_cls', loss[3], self.batch)
        self.writer.add_scalar('Test/mAP_50', map_50, self.batch)
        self.writer.add_scalar('Test/mAP_75', map_75, self.batch)
        self.writer.add_scalar('Test/mAP_coco', map_coco, self.batch)

        if map_50 > self.best_acc:
            #self.params.save(self.checkpoint_filename)
            save_checkpoint(self.params, self.checkpoint_filename, map_50=map_50, map_75=map_75, map_coco=map_coco, loss=loss)
            self.log(f'Saved training state, best accuracy up until now')
            self.best_acc = map_50


if __name__ == '__main__':
    main(TrainEngine, COCOTrainData, COCOValidData, log, ln.data.brambox_collate)


