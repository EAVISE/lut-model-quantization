import lightnet as ln
import torch
from distiller.utils import set_model_input_shape_attr
from distiller.quantization import FakeLinearEstimatedQuantization, FakeNonLinearEstimatedQuantization
from functools import partial

import lightnet_examples.models

__all__ = ['params']

ROOT = 'data/imagenet/'

params = ln.engine.HyperParameters(
    # Network
    input_dimension = (299, 299),
    batch_size_test = 32,
    batch_size_calib = 50,

    calib_size = 50,

    # Dataset
    _valid_set = ROOT + 'val',

    # Preprocessing
    preprocessor_test = 'inception_noaug',

    # Quantization
    quantize_inputs = True,
    bits_inputs = 8,
    signed_inputs = True,
    bits_activations = 8,
    bits_weights = 4,
    bits_bias = None,
    weight_quant_layer_cls=partial(FakeNonLinearEstimatedQuantization),
    act_quant_layer_cls=partial(FakeLinearEstimatedQuantization, estimator='mse', use_pot_scale=True),
)

# Network
params.network = lightnet_examples.models.inception_v3(pretrained=True, transform_input=False, aux_logits=False)
set_model_input_shape_attr(params.network, input_shape=(1, 3, *params.input_dimension))

# Loss
params.loss = torch.nn.CrossEntropyLoss()
