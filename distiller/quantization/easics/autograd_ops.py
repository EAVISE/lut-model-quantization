import torch


class round_ste_fn(torch.autograd.Function):
    """ Autograd function that implements round with a straight through estimator
    """
    @staticmethod
    def forward(ctx, x: torch.Tensor):
        """
        """
        return torch.round(x)

    @staticmethod
    def backward(ctx, grad_y):
        """
        """
        return grad_y


class ceil_ste_fn(torch.autograd.Function):
    """ Autograd function that implements ceil with a straight through estimator
    """
    @staticmethod
    def forward(ctx, x: torch.Tensor):
        """
        """
        return torch.ceil(x)

    @staticmethod
    def backward(ctx, grad_y):
        """
        """
        return grad_y


class proj_ste_fn(torch.autograd.Function):
    """ Autograd function that implements a projection function to a number of arbitrary chosen quantization levels
    """
    @staticmethod
    def forward(ctx, x: torch.Tensor, q_levels: torch.Tensor):
        """ Project values in tensor x onto values in q_levels
        """
        x_flattend = x.view(-1)
        idxs = []

        # split x in chunks of 1M values to limit max memory usage in case x is large
        x_parts = x_flattend.split(1000000)
        for x_part in x_parts:
            idxs.append(torch.argmin((x_part.unsqueeze(0) - q_levels.unsqueeze(1)).abs(), dim=0))
        idxs = torch.cat(idxs)

        x_proj = q_levels[idxs].view(x.shape)

        return x_proj, idxs

    @staticmethod
    def backward(ctx, grad_y, _):
        """ Simple STE: grad_x = grad_y
        """
        return grad_y, None
