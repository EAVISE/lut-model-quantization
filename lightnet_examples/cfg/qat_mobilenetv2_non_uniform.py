import lightnet as ln
import torch
import torch.nn as nn
import distiller
from distiller.utils import set_model_input_shape_attr
from distiller.quantization import FakeNonLinearQEMQuantization, FakeLinearTrainedQuantization
from utils import load_checkpoint, CosineAnnealingWarmupRestarts
from functools import partial

import lightnet_examples.models

# NOTE: use this config file to the train.py and test.py scripts

__all__ = ['params']

ROOT = 'data/imagenet/'

def params(weight_file=[None]):
    """Returns a generator that creates a new network each iteration with different hyper parameters
    """
    bits_weights = [4]
    non_uniform_weights = [True]
    freeze_bn_start = [0]
    freeze_q_levels_start = [1000]
    thresh_lrs = [1e-2]
    aiwq_sampling_start = [1000]
    warmup_len = [0]
    profit_percent = [1.0]
    base_lr = 3e-5

    # cfg for PROFIT
    #bits_weights = [4]
    #non_uniform_weights = [True, True]
    #freeze_bn_start = [0, 0]
    #freeze_q_levels_start = [0, 0]
    #thresh_lrs = [0, 0]
    #aiwq_sampling_start = [-1, -1]
    #warmup_len = [4e4, 4e4]
    #profit_percent = [1/3.]
    #base_lr = 3e-5

    for i in range(len(bits_weights)):

        max_batches = 200000
        _params = ln.engine.HyperParameters(
            # Network
            input_dimension = (224, 224),
            batch_size = 128,
            _mini_batch_size = 128,
            max_batches = max_batches,

            # Dataset
            _train_set = ROOT + 'train',
            _valid_set = ROOT + 'val',

            # Preprocessing
            preprocessor_train = 'default_aug',
            preprocessor_test = 'default_noaug',

            # Quantization
            quantize_inputs = True,
            bits_inputs = 8,
            signed_inputs = True,
            bits_activations = 8,
            bits_weights = 8,
            bits_bias = None,
            weight_quant_layer_cls=partial(FakeNonLinearQEMQuantization, num_bits_q_levels=8, q_level_ema_decay=0.0, q_level_stab_ema_decay=0.999) \
                                  if non_uniform_weights[i] else partial(FakeLinearTrainedQuantization, use_pot_scale=True),
            act_quant_layer_cls=partial(FakeLinearTrainedQuantization, use_pot_scale=True),
            freeze_thresholds_start = 1000,
            freeze_thresholds_period = 50,
            freeze_bn_start = 50000,
            freeze_q_levels_start = 1000,
            freeze_q_levels_period = 50,
            freeze_all_q_levels = 3/4 * max_batches,
            aiwq_sampling_start = 1000,
            aiwq_sampling_period = 25,
            profit_percent = 1.0,
        )

        # Set stage specific quantizer parameters
        _params.bits_weights = bits_weights[i]
        _params.freeze_bn_start = freeze_bn_start[i]
        _params.freeze_q_levels_start = freeze_q_levels_start[i]
        _params.aiwq_sampling_start = aiwq_sampling_start[i]
        _params.profit_percent = profit_percent[i]

        # Construct network
        #_params.network = lightnet_examples.models.mobilenet_v2()
        _params.network = lightnet_examples.models.mobilenet_v2(activation_layer=nn.ReLU, norm_layer=nn.Identity)   # in case cross-layer-equalization was applied
        set_model_input_shape_attr(_params.network, input_shape=(1, 3, *_params.input_dimension))

        # quantize network
        quantizer = distiller.quantization.EasicsQuantizer(_params.network,
                                                           bits_activations=_params.bits_activations,
                                                           bits_weights=_params.bits_weights,
                                                           bits_bias=_params.bits_bias,
                                                           quantize_inputs=_params.quantize_inputs,
                                                           bits_inputs=_params.bits_inputs,
                                                           signed_inputs=_params.signed_inputs,
                                                           weight_quant_layer_cls=_params.weight_quant_layer_cls,
                                                           act_quant_layer_cls=_params.act_quant_layer_cls)
        quantizer.prepare_model()

        # Load initial weights
        if weight_file[0] is not None:
            load_checkpoint(_params.network, weight_file[0])

        # Loss
        _params.loss = torch.nn.CrossEntropyLoss()

        # create parameter groups
        if _params.profit_percent < 1.0:
            weight_parameters = {'params': distiller.quantization.QConvLinear.stable_parameters(_params.network, _params.profit_percent)}
        else:
            weight_parameters = {'params': [v for k, v in _params.network.named_parameters() if 'scale_wt' not in k and 'q_levels' not in k]}
        thresh_parameters = {'params': [v for k, v in _params.network.named_parameters() if 'scale_wt' in k]}

        # Optimizer for weights
        _params.optimizer_weights = torch.optim.Adam(
            [weight_parameters],
            lr = base_lr,
        )

        # Optimizer for thresholds
        _params.optimizer_thresh = torch.optim.Adam(
            [thresh_parameters],
            lr = thresh_lrs[i],
        )

        # Scheduler for weights
        _params.scheduler_weights = ln.engine.SchedulerCompositor(
        #   batch   scheduler
            (0,     CosineAnnealingWarmupRestarts(_params.optimizer_weights,
                                                  # max_batches + 1: otherwise the lr is reset to base_lr at the last batch which we don't want
                                                  first_cycle_steps=_params.max_batches + 1,
                                                  max_lr=base_lr,
                                                  min_lr=0.0,
                                                  warmup_steps=warmup_len[i]))
        )

        # Scheduler for thresholds
        _params.scheduler_thresh = ln.engine.SchedulerCompositor(
        #   batch   scheduler
            (0,     torch.optim.lr_scheduler.StepLR(_params.optimizer_thresh, 1000, gamma=0.5))
        )

        yield _params
