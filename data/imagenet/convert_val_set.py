import sys
import os
import re
import shutil

# Copy the imagenet validation set to a new folder with the same folder structure as is used by the training set
# Usage: python convert_val_set.py /path/to/imagenet/ILSVRC/Data/CLS-LOC/val /path/to/imagenet/ILSVRC/Annotations/CLS-LOC/val/ val

images = sys.argv[1]
annotations = sys.argv[2]
outdir = sys.argv[3]

for anno_f_name in os.listdir(annotations):
    with open(os.path.join(annotations, anno_f_name)) as f:
        text = f.read()
    label = re.findall(r"n\d\d\d\d\d\d\d\d", text)[0]
    image_f_name = anno_f_name.replace('.xml', '.JPEG')
    outdir_label = os.path.join(outdir, label)
    outfile = os.path.join(outdir_label, image_f_name)
    infile = os.path.join(images, image_f_name)

    if not os.path.exists(outdir_label):
        os.makedirs(outdir_label)
    shutil.copy(infile, outfile)
    print(infile, "->", outfile)
