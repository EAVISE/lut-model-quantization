import os
import torch
import lightnet as ln
from distiller.utils import set_model_input_shape_attr
from distiller.quantization import FakeLinearEstimatedQuantization
from functools import partial

__all__ = ['params']

DATA_ROOT = 'data/ms_coco'

params = ln.engine.HyperParameters(
    # Network
    class_label_map = [line.strip() for line in open(os.path.join(DATA_ROOT, 'metadata/coco.names'), 'r')],
    input_dimension = (416, 416),
    batch_size_test = 32,
    batch_size_calib = 50,
    calib_size = 50,

    # Dataset
    _valid_set = os.path.join(DATA_ROOT, 'metadata/5k.part'),

    # Preprocessing
    preprocessor_test = 'yolo_noaug',

    # Quantization
    quantize_inputs = True,
    bits_inputs = 8,
    signed_inputs = True,
    bits_activations = 8,
    bits_weights = 4,
    bits_bias = None,
    weight_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='mse', use_pot_scale=True),
    act_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='mse', use_pot_scale=True),
)

# Network
ln.network.layer.Conv2dBatchReLU.__repr__ = torch.nn.Module.__repr__ # reset custom repr from lightnet for visualization purposes
params.network = ln.models.YoloV2(
    len(params.class_label_map),
    anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)]
)
set_model_input_shape_attr(params.network, input_shape=(1, 3, *params.input_dimension))

# Load initial weights
params.network.load('lightnet_examples/models/yolov2-coco.pt')

# Loss
params.loss = ln.network.loss.RegionLoss(
    len(params.class_label_map),
    params.network.anchors,
    params.network.stride,
)

# Postprocessing
params._post = ln.data.transform.Compose([
    ln.data.transform.GetDarknetBoxes(0.001, params.network.stride, params.network.anchors),
    ln.data.transform.NMS(0.5),
    ln.data.transform.TensorToBrambox(params.class_label_map),
])
