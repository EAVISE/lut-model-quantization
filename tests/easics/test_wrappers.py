import torch
import torch.nn as nn
from distiller.quantization import QAvgPool, FakeLinearConstQuantization, QLeakyReLU


class TestQAvgPool:

    def test_avg_pool2d(self):
        kernel_size = 7
        x = torch.ones((1, 1, kernel_size, kernel_size))
        recip_q = FakeLinearConstQuantization(num_bits=8, is_signed=True, scale=1/32.)
        avgpool = nn.AvgPool2d(kernel_size=kernel_size)

        expected_reciprocal_q = 0.0205078125
        expected_res = torch.ones((1, 1, 1, 1)) * kernel_size**2 * expected_reciprocal_q

        p = QAvgPool(avgpool, recip_q)
        res = p(x)

        assert p.reciprocal_q == torch.tensor(expected_reciprocal_q)
        assert torch.isclose(res, expected_res)

    def test_adaptive_avg_pool2d(self):
        output_size = 1
        input_size = 7
        x = torch.ones((1, 1, input_size, input_size))
        recip_q = FakeLinearConstQuantization(num_bits=8, is_signed=True, scale=1/32.)
        avgpool = nn.AdaptiveAvgPool2d(output_size)

        expected_reciprocal_q = 0.0205078125
        expected_res = torch.ones((1, 1, output_size, output_size)) * input_size**2 * expected_reciprocal_q

        p = QAvgPool(avgpool, recip_q)
        res = p(x)

        assert p.reciprocal_q == torch.tensor(expected_reciprocal_q)
        assert torch.isclose(res, expected_res)


class TestLeakyReLU:

    def test_quant_negative_slope(self):
        negative_slope_q = FakeLinearConstQuantization(num_bits=4, is_signed=True, scale=1/4.)
        leaky_relu = nn.LeakyReLU(0.1)
        r = QLeakyReLU(leaky_relu, None, None, negative_slope_q)
        x_pos = torch.tensor(1.0)
        x_neg = torch.tensor(-1.0)
        res_pos = r(x_pos)
        res_neg = r(x_neg)

        assert torch.isclose(res_pos, x_pos)
        assert torch.isclose(res_neg, torch.tensor(-0.09375))

