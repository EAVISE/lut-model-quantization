import torch
import torch.nn as nn


class FusedConvBatchNorm(nn.Sequential):
    """
    Module wrapper for a convolution module followed by a batchnorm
    """
    def __init__(self, conv, bn):
        super(FusedConvBatchNorm, self).__init__(conv, bn)


class FusedLinearBatchNorm(nn.Sequential):
    """
    Module wrapper for a linear module followed by a batchnorm
    """
    def __init__(self, linear, bn):
        super(FusedLinearBatchNorm, self).__init__(linear, bn)
