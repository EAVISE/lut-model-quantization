import lightnet as ln
import torch
import distiller
import distiller.models
from distiller.quantization import FakeLinearEstimatedQuantization
from functools import partial

__all__ = ['params']

ROOT = 'data/imagenet/'

params = ln.engine.HyperParameters(
    # Network
    input_dimension = (224, 224),
    batch_size_test = 32,
    batch_size_calib = 50,

    calib_size = 50,

    # Dataset
    _valid_set = ROOT + 'val',

    # Preprocessing
    preprocessor_test = 'default_noaug',

    # Quantization
    quantize_inputs = True,
    bits_inputs = 8,
    signed_inputs = True,
    bits_activations = 8,
    bits_weights = 4,
    bits_bias = None,
    weight_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='mse', use_pot_scale=False),
    act_quant_layer_cls=partial(FakeLinearEstimatedQuantization, per_channel=False, estimator='mse', use_pot_scale=False),
)

# Network
params.network = distiller.models.create_model(True, 'imagenet', 'resnet18', False, -1)

# Loss
params.loss = torch.nn.CrossEntropyLoss()
