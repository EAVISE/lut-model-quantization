import torch
import logging
from math import isinf, isnan
from train import QuantizationAwareTrainingEngine, main

from dataset import ImagenetTrainData, ImagenetValidData
from utils import save_checkpoint, test_classifier

log = logging.getLogger('lightnet.Imagenet.train')


class TrainEngine(QuantizationAwareTrainingEngine):

    def task_start(self):
        self.train_loss = []
        self.best_acc = 0

    def process_batch(self, data):
        data, target = data
        data = data.to(self.device)
        target = target.to(self.device, dtype=torch.long)

        output = self.network(data)
        loss = self.loss(output, target) / self.batch_subdivisions
        loss.backward()

        self.train_loss.append(loss.item())

    def task_train_batch(self):
        loss = sum(self.train_loss[-self.batch_subdivisions:])

        self.log(f'{self.batch} Loss:{loss:.5f}')

        if isinf(loss) or isnan(loss):
            log.error('Infinite loss')
            self.sigint = True

    def task_plot_scalars(self):
        if len(self.train_loss) == 0:
            return

        loss = sum(self.train_loss) / (len(self.train_loss) / self.batch_subdivisions)
        self.train_loss = []
        self.writer.add_scalar('Train/Loss', loss, self.batch)

    def test(self):
        self.log('Start testing')
        tp1, tp5, loss = test_classifier(self.network, self.valid_loader, self.device, self.loss, self)
        self.sigint = False

        self.network.train()
        self.loss.train()

        if tp1 is None:
            return

        self.log(f'Loss:{loss:.5f} TOP1:{tp1:.2f}% TOP5:{tp5:.2f}%')
        self.writer.add_scalar('Test/Loss', loss, self.batch)
        self.writer.add_scalar('Test/Top1Accuracy', tp1, self.batch)
        self.writer.add_scalar('Test/Top5Accuracy', tp5, self.batch)

        if tp1 > self.best_acc:
            #self.params.save(self.checkpoint_filename)
            save_checkpoint(self.params, self.checkpoint_filename, tp1=tp1, tp5=tp5, loss=loss)
            self.log(f'Saved training state, best accuracy up until now')
            self.best_acc = tp1


if __name__ == '__main__':
    main(TrainEngine, ImagenetTrainData, ImagenetValidData, log)
