import os
import random
import brambox as bb
import lightnet as ln
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.nn.functional as F

import utils


IMAGENET_MEAN = [0.485, 0.456, 0.406]
IMAGENET_STD = [0.229, 0.224, 0.225]


def preproc_inception_noaug(img, params, central_fraction=0.875):
    """Mimmics tensorflow inception preprocessing from:
    https://github.com/tensorflow/models/blob/r1.12.0/research/slim/preprocessing/inception_preprocessing.py#L244
    """
    img_cropped = transforms.functional.center_crop(img, [int(img.height * central_fraction),
                                                          int(img.width  * central_fraction)])
    x = transforms.ToTensor()(img_cropped)
    x = x.unsqueeze(0)          # add required batch dim for interpolate
    x = F.interpolate(x, params.input_dimension, mode='bilinear', align_corners=False)  # same as tensorflow.resize_bilinear
    x = x[0].sub(0.5).mul(2.0)  # remove batch dim again and scale tensor values
    return x


PREPROCESSORS = {
    'default_aug':   lambda params: transforms.Compose([
                    transforms.RandomResizedCrop(params.input_dimension),
                    transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)]),

    'default_noaug': lambda params: transforms.Compose([
                    transforms.Resize(256),
                    transforms.CenterCrop(params.input_dimension),
                    transforms.ToTensor(),
                    transforms.Normalize(mean=IMAGENET_MEAN, std=IMAGENET_STD)]),

    # From tensorflow inception preprocessing for training (fast mode):
    # https://github.com/tensorflow/models/blob/r1.12.0/research/slim/preprocessing/inception_preprocessing.py#L156
    'inception_aug': lambda params: transforms.Compose([
                    transforms.RandomResizedCrop(params.input_dimension, scale=(0.05, 1.0), ratio=(0.75, 1.33)),
                    transforms.RandomHorizontalFlip(),
                    transforms.ColorJitter(brightness=32. / 255., saturation=(0.5, 1.5)),
                    transforms.ToTensor(),
                    lambda x: x.sub(0.5).mul(2.0)]),

    'inception_noaug': lambda params: transforms.Compose([
                    lambda img: preproc_inception_noaug(img, params)]),

    'darknet_aug':  lambda params: transforms.Compose([
                    ln.data.transform.RandomRotate(params.angle),
                    ln.data.transform.Crop(dimension=params.input_dimension, center=False),
                    ln.data.transform.RandomFlip(params.flip),
                    ln.data.transform.RandomHSV(params.hue, params.saturation, params.value),
                    transforms.ToTensor()]),

    'darknet_noaug': lambda params: transforms.Compose([
                    ln.data.transform.Crop(dimension=params.input_dimension),
                    transforms.ToTensor()]),

    'yolo_aug': lambda params: ln.data.transform.Compose([
                    lambda img: img.convert('RGB'),
                    ln.data.transform.RandomHSV(.1, 1.5, 1.5),
                    ln.data.transform.RandomJitter(.3),
                    ln.data.transform.RandomFlip(.5),
                    ln.data.transform.Letterbox(dimension=params.input_dimension),
                    ln.data.transform.FitAnno(filter_threshold=0.01),
                    transforms.ToTensor()]),

    'yolo_noaug': lambda params: ln.data.transform.Compose([
                    lambda img: img.convert('RGB'),
                    ln.data.transform.Letterbox(dimension=params.input_dimension),
                    ln.data.transform.FitAnno(filter_threshold=0.01),
                    transforms.ToTensor()]),
}


#### ImageNet ####

def get_target_transform(params):
    tf = None
    if hasattr(params, "label_map"):
        label_map = utils.get_darknet_label_map(params.label_map)
        tf = lambda label: label_map[label]
    return tf


class ImagenetTrainData(datasets.ImageFolder):
    def __init__(self, params):

        directory = params.train_set
        tf = PREPROCESSORS[params.preprocessor_train](params)
        target_tf = get_target_transform(params)

        super(ImagenetTrainData, self).__init__(directory, tf, target_tf)


class ImagenetValidData(datasets.ImageFolder):
    def __init__(self, params):

        directory = params.valid_set
        tf = PREPROCESSORS[params.preprocessor_test](params)
        target_tf = get_target_transform(params)

        super(ImagenetValidData, self).__init__(directory, tf, target_tf)


#### MS COCO ####

DATA_ROOT = 'data/ms_coco'


ANNO_FILES = [os.path.join(DATA_ROOT, 'annotations/instances_train2014.json'),
              os.path.join(DATA_ROOT, 'annotations/instances_val2014.json')]


def get_image_path(image_id):
    if 'train' in image_id:
        return os.path.join(DATA_ROOT, f'images/train2014/{image_id}.jpg')
    else:
        return os.path.join(DATA_ROOT, f'images/val2014/{image_id}.jpg')


def parse_annos(anno_filter_list, random_subset_size=-1):
    # Read all annos (train+validation)
    anno = bb.util.concat([bb.io.load('anno_coco', f) for f in ANNO_FILES], sort=False, ignore_index=True)

    # Select correct images (eg. train2014 / val2014 / trainvalno5k / 5k / train2017 / val2017 / ...)
    with open(anno_filter_list) as f:
        img_sel = [os.path.splitext(os.path.basename(line.strip()))[0] for line in f]

    if random_subset_size > 0:
        img_sel = random.sample(img_sel, random_subset_size)

    anno = bb.util.select_images(anno, img_sel)

    return anno


class COCOTrainData(ln.models.BramboxDataset):
    def __init__(self, params):

        transform = PREPROCESSORS[params.preprocessor_train](params)
        anno = parse_annos(params.train_set)

        super().__init__(anno, params.input_dimension, params.class_label_map, get_image_path, transform)


class COCOValidData(ln.models.BramboxDataset):
    def __init__(self, params):

        transform = PREPROCESSORS[params.preprocessor_test](params)
        anno = parse_annos(params.valid_set)

        super().__init__(anno, params.input_dimension, params.class_label_map, get_image_path, transform)


class COCOCalibData(ln.models.BramboxDataset):
    def __init__(self, params):

        transform = PREPROCESSORS[params.preprocessor_test](params)
        anno = parse_annos(params.valid_set, params.calib_size)

        super().__init__(anno, params.input_dimension, params.class_label_map, get_image_path, transform)
