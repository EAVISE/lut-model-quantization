import torch
import numpy as np
from distiller.quantization import ExponentialMovingAverage, CachingFactory, match_dims

#
#   Helper utilities
#
class Dummy:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


#
#   Test cases
#
class TestExponentialMovingAverage:

    def test_numeric(self):
        ema = ExponentialMovingAverage(0.5)
        input_sequence = [2, 4, 6, 8, 10]
        output_sequence = [ema(x) for x in input_sequence]

        np.testing.assert_almost_equal(output_sequence, [2.0, 3.3333333333333335, 4.857142857142857, 6.533333333333333, 8.32258064516129])

    def test_tensor(self):
        ema = ExponentialMovingAverage(0.5)
        input_sequence = torch.Tensor([[2, 2, 2], [4, 4, 4], [6, 6, 6]])
        output_sequence = torch.stack([ema(x) for x in input_sequence])
        assert torch.all(torch.isclose(output_sequence, torch.Tensor([[2.0, 2.0, 2.0],
                                                           [3.3333333333333335, 3.3333333333333335, 3.3333333333333335],
                                                           [4.857142857142857, 4.857142857142857, 4.857142857142857]])))

    def test_decay_zero(self):
        ema = ExponentialMovingAverage(0.0)
        input_sequence = [2, 4, 6, 8, 10]
        output_sequence = [ema(x) for x in input_sequence]

        np.testing.assert_almost_equal(output_sequence, input_sequence)


class TestCachingFactory:

    def test_create_instance(self):
        factory = CachingFactory()
        instance = factory(Dummy, 'foo', bar=3)

        assert instance.args == ('foo',)
        assert instance.kwargs.get('bar') == 3

    def test_new_instance(self):
        factory = CachingFactory()
        instance1 = factory(Dummy, 'foo', bar=3)
        instance2 = factory(Dummy, 'bar', bar=3)
        instance3 = factory(Dummy, 'foo', bar=4)

        assert instance1 is not instance2
        assert instance1 is not instance3

    def test_cached_instance(self):
        factory = CachingFactory()
        instance1 = factory(Dummy, 'foo', bar=3)
        instance2 = factory(Dummy, 'foo', bar=3)

        assert instance1 is instance2


class TestMatchDims:

    def test_activation_tensor_2d_conv(self):
        x = torch.empty(1, 3, 5, 5)
        s = torch.empty(3)
        res = match_dims(s, x, dim=1)
        assert res.shape == torch.Size([3, 1, 1])

    def test_weight_tensor_2d_conv(self):
        x = torch.empty(4, 8, 3, 3)
        s = torch.empty(4)
        res = match_dims(s, x, dim=0)
        assert res.shape == torch.Size([4, 1, 1, 1])

    def test_weight_tensor_linear(self):
        pass

    def test_activation_tensor_linear(self):
        x = torch.empty(1, 8)   # BxL
        s = torch.empty(8)
        res = match_dims(s, x, dim=1)
        assert res.shape == torch.Size([8])
