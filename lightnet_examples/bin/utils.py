import os
import math
import shutil
import socket
from datetime import datetime
import torch
from torch.optim.lr_scheduler import _LRScheduler
import lightnet as ln
import pandas as pd
import brambox as bb
from tqdm import tqdm
from collections.abc import Iterable
import importlib
import logging

log = logging.getLogger('lightnet.Imagenet.train')


def get_darknet_label_map(map_filename):
    """
    Return a list that contains the darknet label indices. You can use this list to translate a regular
    imagenet label index into a darknet label index. For example:
    >>> darknet_label = label_map[imagenet_label]
    """
    label_data = []
    with open(map_filename) as f:
        for index, line in enumerate(f.read().splitlines()):
            label_data.append((index, line))

    sorted_label_data = sorted(label_data, key=lambda x: x[1])

    return [x[0] for x in sorted_label_data]


def save_checkpoint(params, filename, **kwargs):
    """
    Save the state_dict, hyper parameters and optional kwargs to a checkpoint file
    """
    state_dict = params.network.state_dict()
    state = {}
    for k, v in vars(params).items():
        if not k.startswith('_') and k not in ['network', 'loss', 'epoch', 'batch', 'scheduler_thresh']:
            state[k] = v

    checkpoint = {'params': state, 'state_dict': state_dict, **kwargs}
    torch.save(checkpoint, filename)


def load_checkpoint(model, filename, strict=False):
    """Load weights into a model
    """
    log.info(f"Loading weights: {filename}")
    state_dict = torch.load(filename)
    # remove leading 'module.' string in keys if present
    state_dict = {(k[7:] if k.startswith('module.') else k): v for k,v in state_dict['state_dict'].items()}
    model.load_state_dict(state_dict, strict=strict)


def prepare_logdir(cfg_file, logdir, weight_file):
    """
    Generate new subdirectory structure for logging a training session
    Ensure the following folder structure is created:
    logdir
        <machine>_<date_time>_<cfg_file_basename>
            tb
                <tensorboard_log_files>
            checkpoints
                <checkpoints>
            <cfg_file_basename>.py
            std.log
    """
    # Generate new subdirectory structure for logging this training session
    dir_name = socket.gethostname() + datetime.now().strftime("_%B%d_%H_%M_%S_") + os.path.splitext(os.path.basename(cfg_file))[0]
    dir_name = os.path.join(logdir, dir_name)
    tb_dir_name = os.path.join(dir_name, 'tb')
    checkpoints_dir_name = os.path.join(dir_name, 'checkpoints')
    os.makedirs(tb_dir_name)
    os.makedirs(checkpoints_dir_name)
    shutil.copy(cfg_file, dir_name)
    if weight_file:
        shutil.copy(weight_file, os.path.join(checkpoints_dir_name, "init.pt"))

    # log to file too
    logging.basicConfig(filename=os.path.join(dir_name, 'std.log'), filemode='w')

    return tb_dir_name, checkpoints_dir_name


def test_classifier(network, dataloader, device, criteria=None, sigint_ref=None, progress_bar=True, set_eval=True):
    """Test classifier network and return top1 and top5 accuracies together with
    the test loss value if criteria is provided
    """
    # Ensure network and criteria are in eval mode
    if set_eval:
        network.eval()
        if criteria is not None:
            criteria.eval()

    loss_sum = 0
    tp1_sum, tp5_sum = 0,0

    with torch.no_grad():
        for idx, (data, target) in enumerate(tqdm(dataloader) if progress_bar else dataloader):
            data = data.to(device)
            target = target.to(device, dtype=torch.long)

            output = network(data)
            if criteria is not None:
                loss = criteria(output, target)
                loss_sum += loss.item() * len(target)

            tp5 = 0
            r = torch.arange(0, len(target), dtype=torch.long)
            for i in range(5):
                maxes = torch.argmax(output, dim=1)
                output[r, maxes] = -1
                tp5 += (maxes == target).sum().item()
                if i == 0:
                    tp1 = (maxes == target).sum().item()
            tp5_sum += tp5
            tp1_sum += tp1

            # Condition to early return from test loop
            if sigint_ref is not None and sigint_ref.sigint:
                return None, None, None

    loss = None if criteria is None else loss_sum / len(dataloader.dataset)
    tp1 = (tp1_sum * 100) / len(dataloader.dataset)
    tp5 = (tp5_sum * 100) / len(dataloader.dataset)

    return tp1, tp5, loss


def test_detector(network, dataloader, device, post=None, criteria=None, sigint_ref=None, progress_bar=True, set_eval=True):
    """Test object detector network and return top1 and top5 accuracies together with
    the test loss value if criteria is provided
    """
    # Ensure network and criteria are in eval mode
    if set_eval:
        network.eval()
        if criteria is not None:
            criteria.eval()

    loss_dict = {'tot': [], 'coord': [], 'conf': [], 'cls': []}
    anno, det = [], []

    with torch.no_grad():
        for idx, (data, target) in enumerate(tqdm(dataloader) if progress_bar else dataloader):
            data = data.to(device)
            #target = target.to(device, dtype=torch.long)

            output = network(data)
            if criteria is not None:
                criteria(output, target)
                num_img = data.shape[0]
                loss_dict['tot'].append(criteria.loss.item() * num_img)
                loss_dict['coord'].append(criteria.loss_coord.item() * num_img)
                loss_dict['conf'].append(criteria.loss_conf.item() * num_img)
                loss_dict['cls'].append(criteria.loss_cls.item() * num_img)

            if post is not None:
                output = post(output)
                output.image = pd.Categorical.from_codes(output.image, dtype=target.image.dtype)
                det.append(output)
                anno.append(target)

            # Condition to early return from test loop
            if sigint_ref is not None and sigint_ref.sigint:
                return None, None, None, None

    map_50 = map_75 = map_coco = None
    if post is not None:
        anno = bb.util.concat(anno, ignore_index=True, sort=False)
        det = bb.util.concat(det, ignore_index=True, sort=False)
        coco = bb.eval.COCO(det, anno, tqdm=progress_bar)
        # TODO: catch keyboard interrupt
        coco.compute(map_50=True, map_75=True, map_coco=True)
        map_50 = coco.mAP_50 * 100
        map_75 = coco.mAP_75 * 100
        map_coco = coco.mAP_coco * 100

    loss_tot = loss_coord = loss_conf = loss_cls = None
    if criteria is not None:
        loss_tot = sum(loss_dict['tot']) / len(anno.image.cat.categories)
        loss_coord = sum(loss_dict['coord']) / len(anno.image.cat.categories)
        loss_conf = sum(loss_dict['conf']) / len(anno.image.cat.categories)
        loss_cls = sum(loss_dict['cls']) / len(anno.image.cat.categories)

    return map_50, map_75, map_coco, (loss_tot, loss_coord, loss_conf, loss_cls)


# from lightnet repo with some custsom mods
def from_file(path, variable='params', **kwargs):
    """ Create a HyperParameter object from a dictionary in an external configuration file.
    This function will import a file by its path and extract a variable to use as HyperParameters.

    Args:
        path (str or path-like object): Path to the configuration python file
        variable (str, optional): Variable to extract from the configuration file; Default **'params'**
        **kwargs (dict, optional): Extra parameters that are passed to the extracted variable if it is a callable object

    Note:
        The extracted variable can be one of the following:

        - :class:`lightnet.engine.HyperParameters`: This object will simply be returned
        - ``dictionary``: The dictionary will be expanded as the parameters for initializing a new :class:`~lightnet.engine.HyperParameters` object
        - ``callable``: The object will be called with the optional kwargs and should return either a :class:`~lightnet.engine.HyperParameters` object or a ``dictionary``
    """
    try:
        spec = importlib.util.spec_from_file_location('lightnet.cfg', path)
        cfg = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(cfg)
    except AttributeError as err:
        raise ImportError(f'Failed to import the file [{path}]. Are you sure it is a valid python file?') from err

    try:
        params = getattr(cfg, variable)
    except AttributeError as err:
        raise AttributeError(f'Configuration variable [{variable}] not found in file [{path}]') from err

    if callable(params):
        params = params(**kwargs)

    if isinstance(params, ln.engine.HyperParameters) or isinstance(params, Iterable):
        return params
    elif isinstance(params, dict):
        return ln.engine.HyperParameters(**params)
    else:
        raise TypeError(f'Unkown type for configuration variable {variable} [{type(params).__name__}]. This variable should be a dictionary or lightnet.engine.HyperParameters object.')


# Code from https://github.com/katsura-jp/pytorch-cosine-annealing-with-warmup
class CosineAnnealingWarmupRestarts(_LRScheduler):
    """
        optimizer (Optimizer): Wrapped optimizer.
        first_cycle_steps (int): First cycle step size.
        cycle_mult(float): Cycle steps magnification. Default: -1.
        max_lr(float): First cycle's max learning rate. Default: 0.1.
        min_lr(float): Min learning rate. Default: 0.001.
        warmup_steps(int): Linear warmup step size. Default: 0.
        gamma(float): Decrease rate of max learning rate by cycle. Default: 1.
        last_epoch (int): The index of last epoch. Default: -1.
    """

    def __init__(self,
                 optimizer : torch.optim.Optimizer,
                 first_cycle_steps : int,
                 cycle_mult : float = 1.,
                 max_lr : float = 0.1,
                 min_lr : float = 0.001,
                 warmup_steps : int = 0,
                 gamma : float = 1.,
                 last_epoch : int = -1
        ):
        assert warmup_steps < first_cycle_steps

        self.first_cycle_steps = first_cycle_steps # first cycle step size
        self.cycle_mult = cycle_mult # cycle steps magnification
        self.base_max_lr = max_lr # first max learning rate
        self.max_lr = max_lr # max learning rate in the current cycle
        self.min_lr = min_lr # min learning rate
        self.warmup_steps = warmup_steps # warmup step size
        self.gamma = gamma # decrease rate of max learning rate by cycle

        self.cur_cycle_steps = first_cycle_steps # first cycle step size
        self.cycle = 0 # cycle count
        self.step_in_cycle = last_epoch # step size of the current cycle

        super(CosineAnnealingWarmupRestarts, self).__init__(optimizer, last_epoch)

        # set learning rate min_lr
        self.init_lr()

    def init_lr(self):
        self.base_lrs = []
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = self.min_lr
            self.base_lrs.append(self.min_lr)

    def get_lr(self):
        if self.step_in_cycle == -1:
            return self.base_lrs
        elif self.step_in_cycle < self.warmup_steps:
            return [(self.max_lr - base_lr)*self.step_in_cycle / self.warmup_steps + base_lr for base_lr in self.base_lrs]
        else:
            return [base_lr + (self.max_lr - base_lr) \
                    * (1 + math.cos(math.pi * (self.step_in_cycle-self.warmup_steps) \
                                    / (self.cur_cycle_steps - self.warmup_steps))) / 2
                    for base_lr in self.base_lrs]

    def step(self, epoch=None):
        if epoch is None:
            epoch = self.last_epoch + 1
            self.step_in_cycle = self.step_in_cycle + 1
            if self.step_in_cycle >= self.cur_cycle_steps:
                self.cycle += 1
                self.step_in_cycle = self.step_in_cycle - self.cur_cycle_steps
                self.cur_cycle_steps = int((self.cur_cycle_steps - self.warmup_steps) * self.cycle_mult) + self.warmup_steps
        else:
            if epoch >= self.first_cycle_steps:
                if self.cycle_mult == 1.:
                    self.step_in_cycle = epoch % self.first_cycle_steps
                    self.cycle = epoch // self.first_cycle_steps
                else:
                    n = int(math.log((epoch / self.first_cycle_steps * (self.cycle_mult - 1) + 1), self.cycle_mult))
                    self.cycle = n
                    self.step_in_cycle = epoch - int(self.first_cycle_steps * (self.cycle_mult ** n - 1) / (self.cycle_mult - 1))
                    self.cur_cycle_steps = self.first_cycle_steps * self.cycle_mult ** (n)
            else:
                self.cur_cycle_steps = self.first_cycle_steps
                self.step_in_cycle = epoch

        self.max_lr = self.base_max_lr * (self.gamma**self.cycle)
        self.last_epoch = math.floor(epoch)
        for param_group, lr in zip(self.optimizer.param_groups, self.get_lr()):
            param_group['lr'] = lr
