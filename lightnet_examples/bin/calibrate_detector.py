import torch
import logging
import argparse
import distiller
import lightnet as ln

from dataset import COCOValidData, COCOCalibData
from utils import load_checkpoint, save_checkpoint, test_detector


def benchmark_detector(params, loader, device):
    map_50, map_75, map_coco, loss = test_detector(params.network, loader, device, params.post, params.loss)
    log.info(f'loss_tot: {loss[0]:.2f}, loss_coord: {loss[1]:.2f}, loss_conf: {loss[2]:.2f}, loss_cls: {loss[3]:.2f}')
    log.info(f'mAP[0.50]:            {map_50:.2f}%')
    log.info(f'mAP[0.75]:            {map_75:.2f}%')
    log.info(f'mAP[0.50:0.95:0.05]:  {map_coco:.2f}%')
    return map_50, map_75, map_coco, loss


if __name__ == '__main__':
    log = logging.getLogger('lightnet.Imagenet.calibrate')

    parser = argparse.ArgumentParser(description='Calibrate object detector')
    parser.add_argument('weight', help='Path to weight file', default=None, nargs='?')
    parser.add_argument('-n', '--network', help='network config file', required=True)
    parser.add_argument('-o', '--output', help='Path to store calibrated weights', required=True)
    parser.add_argument('-c', '--cuda', action='store_true', help='Use cuda')
    parser.add_argument('-w', '--workers', type=int, help='Number of dataloader workers', default=16)
    parser.add_argument('-d', '--deterministic', action='store_true', help='Enable deterministic behavior')
    parser.add_argument('-t', '--testprior', action='store_true', help='Test prior to calibration')
    args = parser.parse_args()

    # Parse arguments
    if args.deterministic:
        log.info('Deterministic enabled')
        distiller.set_deterministic()

    device = torch.device('cpu')
    if args.cuda:
        if torch.cuda.is_available():
            log.debug('CUDA enabled')
            device = torch.device('cuda')
        else:
            log.error('CUDA not available')

    params = ln.engine.HyperParameters.from_file(args.network)

    # Load weights
    if args.weight is not None:
        load_checkpoint(params.network, args.weight, True)

    # Create dataloaders for test and calibration sets
    valid_loader = torch.utils.data.DataLoader(
        COCOValidData(params),
        batch_size = params.batch_size_test,
        shuffle = False,
        drop_last = False,
        num_workers = args.workers,
        pin_memory = True,
        collate_fn = ln.data.brambox_collate,
    )

    calib_loader = torch.utils.data.DataLoader(
        COCOCalibData(params),
        batch_size = params.batch_size_calib,
        shuffle = False,
        drop_last = False,
        num_workers = args.workers,
        pin_memory = True,
        collate_fn = ln.data.brambox_collate,
    )

    if args.testprior:
        params.to(device)
        # Test network prior to calibration
        log.info('Starting test prior to calibration')
        benchmark_detector(params, valid_loader, device)
        params.to('cpu')

    # Quantize network
    log.info('Quantizing network')
    if hasattr(params, 'quantizer_cls'):
        quantizer = params.quantizer_cls(params.network)
    else:
        # for backward compatibility with old configs: TODO: remove in future
        quantizer = distiller.quantization.EasicsQuantizer(params.network,
                                                           bits_activations=params.bits_activations,
                                                           bits_weights=params.bits_weights,
                                                           bits_bias=params.bits_bias,
                                                           quantize_inputs=params.quantize_inputs,
                                                           bits_inputs=params.bits_inputs,
                                                           signed_inputs=params.signed_inputs,
                                                           weight_quant_layer_cls=params.weight_quant_layer_cls,
                                                           act_quant_layer_cls=params.act_quant_layer_cls)
    quantizer.prepare_model()
    params.to(device)

    # Start calibration
    log.info('Starting calibration of quantizer parameters')

    # enable training mode during calibration, but freeze BN
    params.network.train()
    distiller.quantization.set_frozen_state_batchnorm_stats(params.network, True)
    test_detector(params.network, calib_loader, device, progress_bar=False, set_eval=False)

    print(repr(params.network))

    # Test calibrated network
    log.info('Starting test on calibrated network')
    map_50, map_75, map_coco, loss = benchmark_detector(params, valid_loader, device)

    # Save calibrated weights
    log.info(f'Saving weights to {args.output}')
    save_checkpoint(params, args.output, map_50=map_50, map_75=map_75, map_coco=map_coco, loss=loss)
