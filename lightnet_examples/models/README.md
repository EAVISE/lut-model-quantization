# Content

This directory contains model code that needed modifications and model weights that are not stored in the pytorch cache.
Supported models that are not present in this directory are taken directly from 3rd party libraries without changes.
The next sections describe per model what changed.

## MobileNetV2

The original model from the torchvision model zoo had torch.nn.functional calls in its forward routine.
As stated by distiller, these should be replaced by layer instances to be able to quantize the network.
No structural changes are made to the model.

# Tiny YOLOV2

The original model from Lightnet has 1024 filters in the last but one layer. This model is originally designed
for the Pascal VOC dataset. For the MS COCO dataset, Tiny YOLOV2 uses 512 filters in the last but one layer which
is the only modification we made.
