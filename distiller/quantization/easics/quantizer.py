from functools import partial
from ..quantizer import Quantizer
from .layers import *
from .wrapper_factory import *
from .utils import CachingFactory

import distiller
import distiller.model_transforms as mt


class BasicQuantizer(Quantizer):
    """Configurable quantizer that only quantizes the output of activation layers and the weights
    """
    def __init__(self, model, optimizer=None, bits_activations=None, bits_weights=None,
                 overrides=None, weight_quant_layer_cls=FakeLinearEstimatedQuantization,
                 act_quant_layer_cls=FakeLinearEstimatedQuantization):

        # create adjacency_map
        self.dummy_input = distiller.get_dummy_input(input_shape=model.input_shape)
        summary_graph = distiller.SummaryGraph(model, self.dummy_input)
        adjacency_map = summary_graph.adjacency_map(dedicated_modules_only=False)

        super(BasicQuantizer, self).__init__(model, optimizer=optimizer,
                                                    bits_activations=bits_activations,
                                                    bits_weights=bits_weights,
                                                    overrides=overrides,
                                                    train_with_fp_copy=False,
                                                    adjacency_map=adjacency_map)

        # We only use the replacement factory from the distiller framework, not the parameter quantization
        def param_quant_dummy(param, param_meta):
            return param

        self.param_quantization_fn = param_quant_dummy

        self.replacement_factory[nn.Linear] = \
        self.replacement_factory[nn.Conv1d] = \
        self.replacement_factory[nn.Conv2d] = \
        self.replacement_factory[nn.Conv3d] = QConvLinearFactory(adjacency_map,
                                                                 weight_quant_layer_cls,
                                                                 None,
                                                                 act_quant_layer_cls)
        self.replacement_factory[nn.ReLU] = \
        self.replacement_factory[nn.ReLU6] = QReluFactory(adjacency_map, act_quant_layer_cls)
        self.replacement_factory[nn.LeakyReLU] = QLeakyReLUFactory(adjacency_map, act_quant_layer_cls)

    def _post_prepare_model(self):
        # forward model with dummy input data to initialize dynamic quantizer parameters (initialized in forward method) if any

        # set model to test mode first
        self.model.eval()

        # forward with dummy data on correct device
        self.model(self.dummy_input)


class EasicsQuantizer(Quantizer):
    """Easics configurable quantizer that quantizes all layers and layer types including non conv/linear layers and applies simulated batchnorm folding
    if needed.
    """
    def __init__(self, model, optimizer=None, bits_activations=None, bits_weights=None, bits_bias=None,
                 overrides=None, quantize_inputs=False, bits_inputs=None, signed_inputs=False,
                 weight_quant_layer_cls=FakeLinearEstimatedQuantization,
                 act_quant_layer_cls=FakeLinearEstimatedQuantization,
                 bias_quant_layer_cls=None,
                 freeze_bn=False):

        self.quantize_inputs = quantize_inputs
        self.bits_inputs = bits_inputs
        self.signed_inputs = signed_inputs

        # fuse batch norm layers into convolution layers, prior to calling base ctor
        self.dummy_input = distiller.get_dummy_input(input_shape=model.input_shape)
        summary_graph = distiller.SummaryGraph(model, self.dummy_input)
        adjacency_map = summary_graph.adjacency_map(dedicated_modules_only=False)
        mt.fuse_batch_norms(model, adjacency_map=adjacency_map)

        # Update adjacency_map after fusion
        summary_graph = distiller.SummaryGraph(model, self.dummy_input)
        adjacency_map = summary_graph.adjacency_map(dedicated_modules_only=False)

        super(EasicsQuantizer, self).__init__(model, optimizer=optimizer,
                                                     bits_activations=bits_activations,
                                                     bits_weights=bits_weights,
                                                     bits_bias=bits_bias,
                                                     overrides=overrides,
                                                     train_with_fp_copy=False,
                                                     adjacency_map=adjacency_map)

        self.act_quant_layer_cls = act_quant_layer_cls

        # by default assign the same quantizers as the activations to the bias
        if bias_quant_layer_cls is None:
            bias_quant_layer_cls = act_quant_layer_cls

        # We only use the replacement factory from the distiller framework, not the parameter quantization
        def param_quant_dummy(param, param_meta):
            return param

        self.param_quantization_fn = param_quant_dummy

        self.replacement_factory[distiller.modules.FusedConvBatchNorm] = QConvLinearFoldedBnFactory(adjacency_map,
                                                                                                    weight_quant_layer_cls,
                                                                                                    bias_quant_layer_cls,
                                                                                                    act_quant_layer_cls,
                                                                                                    freeze_bn)
        self.replacement_factory[nn.Linear] = \
        self.replacement_factory[nn.Conv1d] = \
        self.replacement_factory[nn.Conv2d] = \
        self.replacement_factory[nn.Conv3d] = QConvLinearFactory(adjacency_map,
                                                                 weight_quant_layer_cls,
                                                                 bias_quant_layer_cls,
                                                                 act_quant_layer_cls)
        self.replacement_factory[nn.ReLU] = \
        self.replacement_factory[nn.ReLU6] = QReluFactory(adjacency_map, act_quant_layer_cls)
        self.replacement_factory[nn.LeakyReLU] = QLeakyReLUFactory(adjacency_map, act_quant_layer_cls)

        self.replacement_factory[nn.AvgPool2d] = \
        self.replacement_factory[nn.AdaptiveAvgPool2d] = QAvgPoolFactory(adjacency_map, act_quant_layer_cls)
        self.replacement_factory[distiller.modules.EltwiseAdd] = QEltwiseAddFactory(adjacency_map, act_quant_layer_cls)
        self.replacement_factory[distiller.modules.Concat] = QEltwiseConcatFactory(adjacency_map, act_quant_layer_cls)

    def _post_prepare_model(self):

        def inputs_quantize_wrapped_forward(self, input):
            input = self.inputs_quant(input)
            return self.original_forward(input)

        if self.quantize_inputs:
            if isinstance(self.model, nn.DataParallel):
                m = self.model.module
            else:
                m = self.model

            m.inputs_quant = self.act_quant_layer_cls(num_bits=self.bits_inputs, is_signed=self.signed_inputs)

            # Wrap forward function of model instance
            m.original_forward = m.forward
            m.forward = partial(inputs_quantize_wrapped_forward, m)

        # forward model with dummy input data to initialize dynamic quantizer parameters (initialized in forward method) if any

        # set model to test mode first
        self.model.eval()

        # forward with dummy data on correct device
        self.model(self.dummy_input)
