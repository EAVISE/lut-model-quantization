import distiller


class CachingFactory:
    """Create class instances with parameters and return their reference
    In case an instance with that exact parameter set was already created, return its cached reference
    """
    def __init__(self):
        self.cache = {}
        self.kwd_mark = object()

    def __call__(self, cls, *args, **kwargs):
        key = (cls,) + args + (self.kwd_mark,) + tuple(sorted(kwargs.items()))
        if key not in self.cache:
            self.cache[key] = cls(*args, **kwargs)

        return self.cache.get(key)


class ExponentialMovingAverage:
    """Keep track of the EMA of a single number/Tensor
    """
    def __init__(self, decay=0.999):

        self.decay = decay
        self.biased_ema = 0
        self.step = 0

    def __call__(self, value):
        """Exponential moving average calculation
        """
        self.step += 1
        self.biased_ema = self.biased_ema * self.decay + (1 - self.decay) * value
        unbiased_ema = self.biased_ema / (1 - self.decay ** self.step)  # Bias correction
        return unbiased_ema


def match_dims(a, b, dim=0):
    """
    Match the dimentions of a to b and return the reshaped a
    a (torch.Tensor):   1D tensor
    b (torch.Tensor):   (2D, 3D, 4D or 5D tensor)
    dim (int):          Dim where a must be aligned to b
    """
    reshape_params = (b.dim() - 1 - dim) * (1,)
    return a.reshape(-1, *reshape_params)
