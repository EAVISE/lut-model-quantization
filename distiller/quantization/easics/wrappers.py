import torch
import torch.nn as nn
from torch.nn import functional as F
import numpy as np
from .utils import match_dims
from .layers import AIWQ

import distiller


def set_frozen_state_batchnorm_stats(model, state):
    """Freeze/unfreeze all batchnorm statistics in a model
    """
    QConvLinearFoldedBn.set_frozen_state_bn(model, state)


def update_aiwq(model):
    """Set current quantized weights of each QConvLinear module as the previous weights for the AIWQ metric
    calculation module. Call this method just before weight update. The first call will also enable AIWQ
    calculation (if not called at all, no calculation will be performed).
    """
    QConvLinear.update_aiwq(model)


class QConvLinear(nn.Module):
    """Wraps a 1D, 2D, 3D convolution or Linear module and applies necessary quantization ops
    """

    def __init__(self, module, quant_weights=None, quant_bias=None, quant_output=None):
        """
        module (nn.Module):         1D, 2D, 3D convolution or linear module instance to wrap in a quantized module
        quant_weights (nn.Module):  Weight quantizer module instance. If None, the weights are not quantized.
        quant_bias (nn.Module):     Bias quantizer module instance, will also be used for accumulator
                                    quantization. If None, the bias is not quantized.
        quant_output (nn.Module):   Output quantizer module instance. If None, the output is not quantized.
        """
        super(QConvLinear, self).__init__()

        self.param_module = module

        if isinstance(self.param_module, nn.Linear):
            self.param_module_fn = F.linear
        elif isinstance(self.param_module, nn.Conv1d):
            self.param_module_fn = lambda x, w: F.conv1d(x, w, stride=self.param_module.stride,
                                                         padding=self.param_module.padding,
                                                         dilation=self.param_module.dilation,
                                                         groups=self.param_module.groups)
        elif isinstance(self.param_module, nn.Conv2d):
            self.param_module_fn = lambda x, w: F.conv2d(x, w, stride=self.param_module.stride,
                                                         padding=self.param_module.padding,
                                                         dilation=self.param_module.dilation,
                                                         groups=self.param_module.groups)
        elif isinstance(self.param_module, nn.Conv3d):
            self.param_module_fn = lambda x, w: F.conv3d(x, w, stride=self.param_module.stride,
                                                         padding=self.param_module.padding,
                                                         dilation=self.param_module.dilation,
                                                         groups=self.param_module.groups)
        else:
            raise ValueError("Module of type {} is not a supported module by {}",
                             self.param_module.__class__.__name__, self.__class__.__name__)

        self.quant_weights = self.quant_bias = self.quant_output = nn.Identity()
        if quant_weights is not None:
            self.quant_weights = quant_weights
        if quant_bias is not None:
            self.quant_bias = quant_bias
        if quant_output is not None:
            self.quant_output = quant_output

        self.weight_q = None
        self.calc_aiwq = False
        self.aiwq = AIWQ(self.param_module_fn)

    def forward(self, x):
        return self._forward(x, self.param_module.weight, self.param_module.bias)

    def _forward(self, x, weight, bias=None, scale=None):
        """
        We assume that the input is already quantized and therefore directly feed it to
        the wrapped module
        """
        # quant weight
        self.weight = weight.detach()
        weight_q = self.quant_weights(weight)

        # for logging and metric calculation
        self.weight_q = weight_q.detach()

        # apply module from pytorch functional with provided qunatized weights, no bias
        y = self.param_module_fn(x, weight_q)

        # update AIWQ metric if requested
        if self.calc_aiwq:
            self.aiwq(x, y)
            self.calc_aiwq = False

        # apply optional output scaling before bias
        if scale is not None:
            y *= match_dims(scale, y, dim=1)

        # apply bias manually if applicable
        if bias is not None:
            # broadcast bias to the same shape of y, concat expanded bias and y onto the batch dim
            # and run them through the bias quantizer. This forces y and bias to share the same
            # quantizer parameters.
            y_bias = torch.cat((y, self._broadcast_bias(bias, y)), dim=0)
            y_bias_q = self.quant_bias(y_bias)

            # split up quantized y and bias tensors and apply bias to y
            y_q, bias_q = torch.split(y_bias_q, y_bias_q.size(0) // 2, dim=0)
            y = y_q + bias_q

        return self.quant_output(y)

    def _broadcast_bias(self, bias, x):
        """
        Broadcast bias shape to the shape of x so that bias can be concatenated to x
        The bias values are broadcasted onto the channel dim
        """
        reshape_params = (x.dim() - 2) * (1,)
        return bias.reshape(-1, *reshape_params).expand_as(x)

    @classmethod
    def update_aiwq(cls, model):
        """Trigger an update of the AIWQ metrics within each QConvLinear module
        Call this method just before weight update (make sure no model forward occurs between the call to this function and the weight update).
        NOTE: you can call this method every N iterations with N > 1 to reduce overall computation time
        """

        for module in model.modules():
            if isinstance(module, cls):
                module.calc_aiwq = True
                module.aiwq.prev_weight = module.weight_q

    @classmethod
    def stable_parameters(cls, model, percent):
        """Return a list parameters with stable conv/linear weights based on the module's AIWQ metric.
        The weights of a conv/linear module with low AIWQ are considered stable.
        Percent indicates the percentage of weights params that are most stable. If for example percent is 0.2,
        the returned parameters only contain 20% of the weight parameters with the lowest AIWQ.

        Each conv/lin module has only 1 aiwq metric
        """

        assert percent >= 0 and percent <= 1

        # Select conv/linear weights and sort them according to their AIWQ metric
        weight_parameters = []
        bias_parameters = []
        for name, module in model.named_modules():
            if not isinstance(module, cls):
                continue

            w_params = []
            for k, v in module.named_parameters():
                # both (folded) batchnorm weight (gamma) and conv/linear weights need to be frozen since
                # gamma also influences the folded weights
                if '.weight' in k:
                    w_params.append(v)
                # don't include quantizer parameters since they use a different optimiser
                elif '.bias' in k:
                    bias_parameters.append(v)
            weight_parameters.append((module.aiwq.metric.item(), w_params,))

        sorted_weight_parameters = sorted(weight_parameters, key=lambda x: x[0])
        num_params = round(percent * len(sorted_weight_parameters))

        stable_weight_parameters = []
        for w in sorted_weight_parameters[:num_params]:
            stable_weight_parameters.extend(w[1])

        return stable_weight_parameters + bias_parameters


class QConvLinearFoldedBn(QConvLinear):
    """Wraps a convolution or linear module followed by a batchnorm module and simulates batchnorm folding during
    training while applying the necessary quantization ops
    """
    def __init__(self, module, bn_module, freeze_bn=False, quant_weights=None, quant_bias=None, quant_output=None):
        """
        module (nn.Module):         1D, 2D, 3D convolution or linear module instance to wrap in a quantized module
        bn_module (nn.Module):      1D, 2D or 3D batchnorm module instance to wrap
        freeze_bn (bool):           Freeze batchnorm statistics from the start
        quant_weights (nn.Module):  Weight quantizer module instance. If None, the weights are not quantized.
        quant_bias (nn.Module):     Bias quantizer module instance, will also be used for accumulator
                                    quantization. If None, the bias is not quantized.
        quant_output (nn.Module):   Output quantizer module instance. If None, the output is not quantized.
        """
        super(QConvLinearFoldedBn, self).__init__(module, quant_weights, quant_bias, quant_output)

        self.bn_module = bn_module
        self.freeze_bn = freeze_bn
        self.permanently_folded = False

    def forward(self, x):
        """
        Implemented according to https://arxiv.org/pdf/1806.08342.pdf section 3.2.2.
        """
        # during valiadation, always use frozen BN
        if self.freeze_bn:
            weight_folded, bias_folded = self.get_folded_weight_and_bias(permanent=False)
            return self._forward(x, weight_folded, bias_folded)

        if not self.training:
            weight_folded, bias_folded = self.get_folded_weight_and_bias(permanent=False)
            return self._forward(x, weight_folded, bias_folded)

        weight, bias = self.param_module.weight, self.param_module.bias
        gamma, beta = self.bn_module.weight, self.bn_module.bias

        # forward module with unquantized weights, calculate batch statistics and
        # update running statistics based on the output of the module
        batch_mean, batch_var = self.batch_stats(self.param_module_fn(x, weight), bias)
        recip_sigma_batch = torch.rsqrt(batch_var + self.bn_module.eps)     # rsqrt(x) = 1/sqrt(x)
        sigma_running = torch.sqrt(self.bn_module.running_var + self.bn_module.eps)

        # calculate folded weights: W * gamma/running_sigma
        weight_folded = weight * match_dims(gamma / sigma_running, weight)
        # calculate correction scale: running_sigma/batch_sigma
        scale = sigma_running * recip_sigma_batch
        # calculate folded bias: beta - gamma * batch_mean/batch_sigma
        bias_folded = beta - gamma * batch_mean * recip_sigma_batch

        return self._forward(x, weight_folded, bias_folded, scale)

    def get_folded_weight_and_bias(self, permanent=False):
        """Calculate folded weights and bias and return them
        If permanent == True, after calculation, the folded weight and bias will be assigned
        to the conv weight an bias. Note that this cannot be reversed.
        """
        if self.permanently_folded:
            return self.param_module.weight, self.bn_module.bias

        weight, bias = self.param_module.weight, self.param_module.bias
        gamma, beta = self.bn_module.weight, self.bn_module.bias

        # calculate folded weights
        recip_sigma_running = torch.rsqrt(self.bn_module.running_var + self.bn_module.eps)
        weight_folding_multiplier = match_dims(gamma * recip_sigma_running, weight)

        # calculate folded bias: beta - gamma * (running_mean-bias)/running_sigma
        corrected_mean = self.bn_module.running_mean - (bias if bias is not None else 0)
        bias_folding_offset = - gamma * corrected_mean * recip_sigma_running

        if not permanent:
            return weight * weight_folding_multiplier, beta + bias_folding_offset

        self.param_module.weight *= weight_folding_multiplier
        self.bn_module.bias += bias_folding_offset
        # ensure batch norm layer behaves like identity layer to ensure that folding in a second
        # training stage will not alter the weights and bias
        self.bn_module.weight.fill_(1)
        self.bn_module.running_var.fill_(1 - self.bn_module.eps)
        self.bn_module.running_mean.fill_(0)
        self.permanently_folded = True

        return self.param_module.weight, self.bn_module.bias

    def batch_stats(self, x, bias=None):
        """
        Get the batch mean and variance of x and updates the BatchNorm's running mean and average.
        We compute them manually since batch statistics cannot be retrieved from a pytorch batchnorm layer.
        Args:
            x (torch.Tensor): input batch.
            bias (torch.Tensor): the bias that is to be applied to the batch.
        Returns:
            (mean,variance)
        Note:
            In case of `nn.Linear`, x may be of shape (N, C, L) or (N, L)
            where N is batch size, C is number of channels, L is the features size.
            The batch norm computes the stats over C in the first case or L on the second case.
            The batch normalization layer is
            (`nn.BatchNorm1d`)[https://pytorch.org/docs/stable/nn.html#batchnorm1d]

            In case of `nn.Conv2d`, x is of shape (N, C, H, W)
            where H,W are the image dimensions, and the batch norm computes the stats over C.
            The batch normalization layer is
            (`nn.BatchNorm2d`)[https://pytorch.org/docs/stable/nn.html#batchnorm2d]
        """
        channel_size = self.bn_module.num_features
        self.bn_module.num_batches_tracked += 1

        # Calculate current batch stats
        batch_mean = x.transpose(0, 1).contiguous().view(channel_size, -1).mean(1)
        # BatchNorm currently uses biased variance (without Bessel's correction) as was discussed at
        # https://github.com/pytorch/pytorch/issues/1410
        #
        # also see the source code itself:
        # https://github.com/pytorch/pytorch/blob/master/aten/src/ATen/native/Normalization.cpp#L216
        batch_var = x.transpose(0, 1).contiguous().view(channel_size, -1).var(1, unbiased=False)

        # Update running stats
        with torch.no_grad():
            biased_batch_mean = batch_mean + (bias if bias is not None else 0)
            # However - running_var is updated using unbiased variance!
            # https://github.com/pytorch/pytorch/blob/master/aten/src/ATen/native/Normalization.cpp#L223
            n = x.numel() / channel_size
            corrected_var = batch_var * (n / (n - 1))
            momentum = self.bn_module.momentum
            if momentum is None:
                # momentum is None - we compute a cumulative moving average
                # as noted in https://pytorch.org/docs/stable/nn.html#batchnorm2d
                momentum = 1. / float(self.bn_module.num_batches_tracked)
            self.bn_module.running_mean.mul_(1 - momentum).add_(momentum * biased_batch_mean)
            self.bn_module.running_var.mul_(1 - momentum).add_(momentum * corrected_var)

        return batch_mean, batch_var

    @classmethod
    def set_frozen_state_bn(cls, model, state):
        """Freeze/unfreeze all batchnorm statistics in a model
        """
        for module in model.modules():
            if isinstance(module, cls):
                module.freeze_bn = state


class QRelu(nn.Module):
    """Wraps a ReLU or ReLU6 activation module and applies necessary quantization ops
    """
    def __init__(self, module, quant_activations=None):
        """
        module (nn.Module):             ReLU or ReLU6 module instance to wrap in a quantized module
        quant_activations (nn.Module):  Activations quantizer module instance. If None, the activations
                                        are not quantized.
        """
        super(QRelu, self).__init__()

        self.activation = module

        if not isinstance(self.activation, nn.ReLU) and \
           not isinstance(self.activation, nn.ReLU6):
            raise ValueError("Module of type {} is not a supported module by {}",
                             self.activation.__class__.__name__, self.__class__.__name__)

        self.quant_act = nn.Identity()
        if quant_activations is not None:
            self.quant_act = quant_activations

    def forward(self, x):
        return self.quant_act(self.activation(x))


class QLeakyReLU(nn.Module):
    """Wraps a LeakyRelu module and applies necessary quantization ops
    """
    def __init__(self, module, quant_input=None, quant_output=None, quant_negative_slope=None):
        """
        module (nn.Module): LeakyReLU
        quant_input (nn.Module):  Input quantizer module instance. If None, the inputs are not quantized.
        quant_output (nn.Module):  Output quantizer module instance. If None, the output is not quantized.
        quant_negative_slope (nn.Module):   Negative slope quantizer
        """

        super(QLeakyReLU, self).__init__()

        # quantize negative slope if applicable
        if quant_negative_slope is not None:
            module.negative_slope = quant_negative_slope(torch.Tensor([module.negative_slope])).item()

        self.activation = module

        self.quant_input = nn.Identity()
        if quant_input is not None:
            self.quant_input = quant_input

        self.quant_output = nn.Identity()
        if quant_output is not None:
            self.quant_output = quant_output

    def forward(self, x):
        return self.quant_output(self.activation(self.quant_input(x)))


class QEltwiseAdd(nn.Module):
    """Wraps an element-wise add module from distiller and applies necessary quantization ops
    """
    def __init__(self, module, quant_input=None, quant_output=None):
        """
        module (nn.Module): distiller.module.EltWiseAdd
        quant_input (nn.Module):  Input quantizer module instance. If None, the inputs are not quantized.
        quant_output (nn.Module):  Output quantizer module instance. If None, the output is not quantized.
        """
        super(QEltwiseAdd, self).__init__()

        self.op = module

        if not isinstance(self.op, distiller.modules.EltwiseAdd):
            raise ValueError(f"Module of type {self.op.__class__.__name__} is not a supported module by {self.__class__.__name__}")

        self.quant_input = self.quant_output = nn.Identity()
        if quant_input is not None:
            self.quant_input = quant_input
        if quant_output is not None:
            self.quant_output = quant_output

    def forward(self, *xs):
        # Concat all inputs on the batch dimension and run them through the quantizer.
        # This forces shared quantizer parameters on all inputs
        batch_size = xs[0].size(0)
        xs_q = self.quant_input(torch.cat(xs, dim=0))
        xs_q = torch.split(xs_q, batch_size, dim=0)
        return self.quant_output(self.op(*xs_q))


class QEltwiseConcat(nn.Module):
    """Wraps a concat module from distiller and applies necessary quantization ops
    Inputs are not quantized since concat does not change the tensor values.
    """
    def __init__(self, module, quant_output=None):
        """
        module (nn.Module): distiller.module.Concat
        quant_output (nn.Module):  Output quantizer module instance. If None, the output is not quantized.
        """
        super(QEltwiseConcat, self).__init__()

        self.op = module

        if not isinstance(self.op, distiller.modules.Concat):
            raise ValueError(f"Module of type {self.op.__class__.__name__} is not a supported module by {self.__class__.__name__}")

        self.quant_output = nn.Identity()
        if quant_output is not None:
            self.quant_output = quant_output

    def forward(self, *xs):
        return self.quant_output(self.op(*xs))


class QAvgPool(nn.Module):
    """Wraps an AvgPool2d or AdaptiveAvgPool2d module and applies necessary quantization ops
    """

    def __init__(self, module, quant_reciprocal=None, quant_output=None):
        """
        module (nn.Module): nn.AvgPool2d, or nn.AdaptiveAvgPool2d instance
        quant_reciprocal (nn.Module):   Reciprocal constant (1/K^2 with K the kernel size) quantizer
                                        module instance. If None, the reciprocal is not quantized
        quant_output (nn.Module):  Output quantizer module instance. If None, the output is not quantized.
        """
        super(QAvgPool, self).__init__()

        self.avgpool = module

        self.quant_reciprocal = self.quant_output = nn.Identity()
        if quant_reciprocal is not None:
            self.quant_reciprocal = quant_reciprocal

        if quant_output is not None:
            self.quant_output = quant_output

        self.reciprocal_q = None
        if isinstance(module, nn.AvgPool2d):
            # pre-calculate quantized reciprocal since its constant
            ks = self.avgpool.kernel_size
            self.reciprocal_q = self.calc_reciprocal_quant(ks if isinstance(ks, tuple) else 2 * (ks,))
            self.module_fn = self.avg_pool2d
        elif isinstance(module, nn.AdaptiveAvgPool2d):
            self.module_fn = self.adaptive_avg_pool2d
        else:
            raise ValueError("Module of type {} is not a supported module by {}",
                             self.op.__class__.__name__, self.__class__.__name__)

    def avg_pool2d(self, x):
        return self._avg_pool2d(x, self.avgpool.kernel_size,
                                   self.avgpool.stride,
                                   self.avgpool.padding,
                                   self.reciprocal_q)

    def adaptive_avg_pool2d(self, x):
        """
        Apply adaptive avg pooling with regular avg pooling.
        """
        ks = self.calc_kernel_size(x, self.avgpool.output_size)
        self.reciprocal_q = self.calc_reciprocal_quant(ks, x.device)
        return self._avg_pool2d(x, ks, ks, 0, self.reciprocal_q)

    def _avg_pool2d(self, x, ks, stride, padding, reciprocal):
        """Implement 2d avg pool by depth-wise convolution
        This way, we can control the reciprocal
        """
        if not isinstance(ks, tuple):
            ks = 2 * (ks,)
        # construct weight tensor filled with reciprocal values
        num_channels = x.size(-3)
        weights = x.new_ones((num_channels, 1, ks[0], ks[1])) * reciprocal
        return F.conv2d(x, weights, stride=stride,
                                    padding=padding,
                                    groups=num_channels)

    def calc_kernel_size(self, x, output_size):
        dims = 2
        if not isinstance(output_size, tuple):
            output_size = dims * (output_size,)

        input_size = x.shape[-dims:]
        return tuple(int(np.ceil(input_size[i] / output_size[i]))
                     for i in range(dims))

    def calc_reciprocal_quant(self, ks, device=torch.device('cpu')):
        # calculate reciprocal
        kernel_size_prod = np.prod(ks)
        reciprocal = torch.Tensor([1. / kernel_size_prod])
        reciprocal = reciprocal.to(device)
        return self.quant_reciprocal(reciprocal).item()

    def forward(self, x):
        return self.quant_output(self.module_fn(x))
