import torch
import torch.nn as nn
from distiller.quantization import (FakeLinearSymmetricQuantCore, FakeLinearAsymmetricQuantCore,
                                    FakeLinearTrainedQuantization, FakeLinearEstimatedQuantization,
                                    FakeLinearPPQuantization, FakeNonLinearQEMQuantization, AIWQ)
import numpy as np

#
#   Helper functions
#
def calc_log2_scale_grad(x, scale, bits, is_signed, use_pot_scale=False):
    """
    See https://arxiv.org/pdf/1903.08066.pdf equation 7
    """
    if is_signed:
        div = np.power(2, bits-1)
        n = -np.power(2, bits-1)
        p = div -1
    else:
        div = np.power(2, bits)
        n = 0
        p = div -1

    if use_pot_scale:
        s = np.power(2, np.ceil(np.log2(scale))) / div
    else:
        s = scale / div

    if np.round(x/s) < n:
        term = n
    elif np.round(x/s) > p:
        term = p
    else:
        term = np.round(x/s) - x/s

    return torch.Tensor([float(s * np.log(2) * term)])


def calc_scale_grad(x, scale, bits, is_signed, use_pot_scale=False):
    """
    See https://arxiv.org/pdf/1903.08066.pdf equation 7
    """
    if is_signed:
        div = np.power(2, bits-1)
        n = -np.power(2, bits-1)
        p = div -1
    else:
        div = np.power(2, bits)
        n = 0
        p = div -1

    if use_pot_scale:
        s = np.power(2, np.ceil(np.log2(scale))) / div
    else:
        s = scale / div

    if np.round(x/s) < n:
        term = n
    elif np.round(x/s) > p:
        term = p
    else:
        term = np.round(x/s) - x/s

    return torch.Tensor([float(term / div)])


#
#   Test cases
#
class TestFakeLinearSymmetricQuantCore:

    def test_forward_signed_positive_input(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([1.4])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.5]))
        x = torch.Tensor([1.75])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.75]))

    def test_forward_signed_negative_input(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([-1.4])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([-1.5]))
        x = torch.Tensor([-2.0])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([-2.0]))

    def test_forward_signed_positive_clamp(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([2.0])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.75]))

    def test_forward_signed_negative_clamp(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([-2.5])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([-2.0]))

    def test_forward_unsigned_positive_input(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=False)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([1.4])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.375]))
        x = torch.Tensor([1.875])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.875]))

    def test_forward_unsigned_positive_clamp(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=False)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([2.0])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([1.875]))

    def test_forward_unsigned_negative_clamp(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=False)
        scale = torch.Tensor([2.0])
        x = torch.Tensor([-2.0])
        res = q(x, scale)
        assert torch.isclose(res, torch.Tensor([0.0]))

    def test_forward_per_channel_signed_positive_activation(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True)
        scale = torch.Tensor([2.0, 1.0])
        x = torch.Tensor([[[1.4, 1.5], [0.9, 0.8]]])
        res = q(x, scale)
        assert torch.allclose(res, torch.Tensor([[1.5, 1.5], [0.875, 0.75]]))

    def test_forward_linear_per_channel_signed_positive_weights(self):
        q = FakeLinearSymmetricQuantCore(4, is_signed=True, per_channel_dim=0)
        scale = torch.Tensor([2.0, 1.0])
        # weight tensor with 2 filters, 2 channels and 1x1 kernel
        w = torch.Tensor([[[[1.4]], [[1.5]]], [[[0.9]], [[0.8]]]])
        res = q(w, scale)
        assert torch.allclose(res, torch.Tensor([[[[1.5]], [[1.5]]], [[[0.875]], [[0.75]]]]))

    def test_backward_signed_positive_input(self):
        bits = 4; is_signed = True
        q = FakeLinearSymmetricQuantCore(bits, is_signed)
        scale = torch.Tensor([2.0]).requires_grad_()
        x = torch.Tensor([1.4]).requires_grad_()
        y = q(x, scale)
        y.backward()

        # check grad of input x
        assert torch.isclose(torch.Tensor([1.0]), x.grad)

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
        assert torch.isclose(expected_scale_grad, scale.grad)

    def test_backward_signed_positive_clamp(self):
        bits = 4; is_signed = True
        q = FakeLinearSymmetricQuantCore(bits, is_signed)
        scale = torch.Tensor([2.0]).requires_grad_()
        x = torch.Tensor([2.0]).requires_grad_()
        y = q(x, scale)
        y.backward()

        # check grad of input x
        assert torch.isclose(torch.Tensor([0.0]), x.grad)

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
        assert torch.isclose(expected_scale_grad, scale.grad)

    def test_backward_signed_negative_clamp(self):
        bits = 4; is_signed = True
        q = FakeLinearSymmetricQuantCore(bits, is_signed)
        scale = torch.Tensor([2.0]).requires_grad_()
        x = torch.Tensor([-2.5]).requires_grad_()
        y = q(x, scale)
        y.backward()

        # check grad of input x
        assert torch.isclose(torch.Tensor([0.0]), x.grad)

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
        assert torch.isclose(expected_scale_grad, scale.grad)

    def test_backward_unsigned_positive_input(self):
        bits = 4; is_signed = False
        q = FakeLinearSymmetricQuantCore(bits, is_signed)
        scale = torch.Tensor([2.0]).requires_grad_()
        x = torch.Tensor([1.4]).requires_grad_()
        y = q(x, scale)
        y.backward()

        # check grad of input x
        assert torch.isclose(torch.Tensor([1.0]), x.grad)

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
        assert torch.isclose(expected_scale_grad, scale.grad)


class TestFakeLinearAsymmetricQuantCore:

    def test_forward_symm_positive_input(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([4.0])
        zero = torch.Tensor([8])
        x = torch.Tensor([1.4])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.5]))
        x = torch.Tensor([1.75])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.75]))

    def test_forward_symm_negative_input(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([4.0])
        zero = torch.Tensor([8])
        x = torch.Tensor([-1.4])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([-1.5]))
        x = torch.Tensor([-2.0])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([-2.0]))

    def test_forward_symm_positive_clamp(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([4.0])
        zero = torch.Tensor([8])
        x = torch.Tensor([2.0])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.75]))

    def test_forward_symm_negative_clamp(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([4.0])
        zero = torch.Tensor([8])
        x = torch.Tensor([-2.5])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([-2.0]))

    def test_forward_positive_bias_positive_input(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([2.0])
        zero = torch.Tensor([0])
        x = torch.Tensor([1.4])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.375]))
        x = torch.Tensor([1.875])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.875]))

    def test_forward_positive_bias_positive_clamp(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([2.0])
        zero = torch.Tensor([0])
        x = torch.Tensor([2.0])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([1.875]))

    def test_forward_negative_bias_positive_clamp(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([2.0])
        zero = torch.Tensor([15])
        x = torch.Tensor([1.0])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([0.0]))

    def test_forward_negative_bias_negative_clamp(self):
        q = FakeLinearAsymmetricQuantCore(4)
        scale = torch.Tensor([2.0])
        zero = torch.Tensor([16])
        x = torch.Tensor([-2.5])
        res = q(x, scale, zero)
        assert torch.isclose(res, torch.Tensor([-2.0]))

    #def test_forward_per_channel_signed_positive_activation(self):
    #    q = FakeLinearSymmetricQuantCore(4, is_signed=True)
    #    scale = torch.Tensor([2.0, 1.0])
    #    x = torch.Tensor([[[1.4, 1.5], [0.9, 0.8]]])
    #    res = q(x, scale)
    #    assert torch.allclose(res, torch.Tensor([[1.5, 1.5], [0.875, 0.75]]))

    #def test_forward_linear_per_channel_signed_positive_weights(self):
    #    q = FakeLinearSymmetricQuantCore(4, is_signed=True, per_channel_dim=0)
    #    scale = torch.Tensor([2.0, 1.0])
    #    # weight tensor with 2 filters, 2 channels and 1x1 kernel
    #    w = torch.Tensor([[[[1.4]], [[1.5]]], [[[0.9]], [[0.8]]]])
    #    res = q(w, scale)
    #    assert torch.allclose(res, torch.Tensor([[[[1.5]], [[1.5]]], [[[0.875]], [[0.75]]]]))

    #def test_backward_signed_positive_input(self):
    #    bits = 4; is_signed = True
    #    q = FakeLinearSymmetricQuantCore(bits, is_signed)
    #    scale = torch.Tensor([2.0]).requires_grad_()
    #    x = torch.Tensor([1.4]).requires_grad_()
    #    y = q(x, scale)
    #    y.backward()

    #    # check grad of input x
    #    assert torch.isclose(torch.Tensor([1.0]), x.grad)

    #    # check grad of parameter scale_wt by manually calculating it with numpy
    #    expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
    #    assert torch.isclose(expected_scale_grad, scale.grad)

    #def test_backward_signed_positive_clamp(self):
    #    bits = 4; is_signed = True
    #    q = FakeLinearSymmetricQuantCore(bits, is_signed)
    #    scale = torch.Tensor([2.0]).requires_grad_()
    #    x = torch.Tensor([2.0]).requires_grad_()
    #    y = q(x, scale)
    #    y.backward()

    #    # check grad of input x
    #    assert torch.isclose(torch.Tensor([0.0]), x.grad)

    #    # check grad of parameter scale_wt by manually calculating it with numpy
    #    expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
    #    assert torch.isclose(expected_scale_grad, scale.grad)

    #def test_backward_signed_negative_clamp(self):
    #    bits = 4; is_signed = True
    #    q = FakeLinearSymmetricQuantCore(bits, is_signed)
    #    scale = torch.Tensor([2.0]).requires_grad_()
    #    x = torch.Tensor([-2.5]).requires_grad_()
    #    y = q(x, scale)
    #    y.backward()

    #    # check grad of input x
    #    assert torch.isclose(torch.Tensor([0.0]), x.grad)

    #    # check grad of parameter scale_wt by manually calculating it with numpy
    #    expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
    #    assert torch.isclose(expected_scale_grad, scale.grad)

    #def test_backward_unsigned_positive_input(self):
    #    bits = 4; is_signed = False
    #    q = FakeLinearSymmetricQuantCore(bits, is_signed)
    #    scale = torch.Tensor([2.0]).requires_grad_()
    #    x = torch.Tensor([1.4]).requires_grad_()
    #    y = q(x, scale)
    #    y.backward()

    #    # check grad of input x
    #    assert torch.isclose(torch.Tensor([1.0]), x.grad)

    #    # check grad of parameter scale_wt by manually calculating it with numpy
    #    expected_scale_grad = calc_scale_grad(x.detach().numpy(), scale.item(), bits, is_signed)
    #    assert torch.isclose(expected_scale_grad, scale.grad)


class TestFakeLinearTrainedQuantization:

    def test_scale_init(self):
        q = FakeLinearTrainedQuantization(init_scale=2.0, num_bits=4, is_signed=True)
        assert q.scale_wt == 1.0

    def test_frozen(self):
        q = FakeLinearTrainedQuantization()
        assert q.frozen == False
        assert q.scale_wt.requires_grad == True

        q.frozen = True
        assert q.frozen == True
        assert q.scale_wt.requires_grad == False

    def test_frozen_channels_per_layer(self):
        q = FakeLinearTrainedQuantization()

        q.frozen_channels = torch.BoolTensor([True])
        assert q.frozen_channels == torch.BoolTensor([True])
        assert q.frozen == True

    def test_frozen_channels_per_channel(self):
        q = FakeLinearTrainedQuantization(per_channel=True)

        # None before first forward
        assert q.frozen_channels is None

        q(torch.empty(1, 3, 4, 4))
        assert torch.all(torch.eq(q.frozen_channels, torch.tensor(3 * [False])))

        val = torch.BoolTensor([False, True, False])
        q.frozen_channels = val
        assert torch.all(torch.eq(q.frozen_channels, val))
        assert q.frozen == False

        val = torch.BoolTensor([True, True, True])
        q.frozen_channels = val
        assert torch.all(torch.eq(q.frozen_channels, val))
        assert q.frozen == True

        val = torch.BoolTensor([False, False, False])
        q.frozen_channels = val
        assert torch.all(torch.eq(q.frozen_channels, val))
        assert q.frozen == False

    def test_repr(self):
        q = FakeLinearTrainedQuantization(num_bits=4, is_signed=True, init_scale=2.0)
        assert str(q) == "FakeLinearTrainedQuantization(num_bits=4, is_signed=True, frozen=False, used_scales=[2.], per_channel=False)"
        q = FakeLinearTrainedQuantization(is_signed=False, init_scale=2.0, per_channel=True)
        q(torch.randn(1, 3, 4, 4))
        assert str(q) == "FakeLinearTrainedQuantization(num_bits=8, is_signed=False, frozen=False, used_scales=[2.], per_channel=True)"

    def test_repr_pot_scale(self):
        # printed scale is the power-of-two ceiled scale
        q = FakeLinearTrainedQuantization(num_bits=4, is_signed=True, use_pot_scale=True, init_scale=3.0)
        assert str(q) == "FakeLinearTrainedQuantization(num_bits=4, is_signed=True, frozen=False, used_scales=[4.], per_channel=False)"

    def test_integration_signed_positive_input(self):
        q = FakeLinearTrainedQuantization(num_bits=4, is_signed=True, init_scale=2.0)
        x = torch.Tensor([1.4])
        res = q(x)
        assert torch.isclose(res, torch.Tensor([1.5]))

    def test_backward_signed_positive_input(self):
        scale = 5; bits = 4; is_signed = True
        q = FakeLinearTrainedQuantization(num_bits=bits, is_signed=is_signed, init_scale=scale)
        x = torch.Tensor([1.4]).requires_grad_()
        y = q(x)
        y.backward()

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_log2_scale_grad(x.detach().numpy(), scale, bits, is_signed)
        assert torch.isclose(expected_scale_grad, q.scale_wt.grad)

    def test_backward_signed_positive_input_pot_scale(self):
        scale = 5; bits = 4; is_signed = True
        q = FakeLinearTrainedQuantization(num_bits=bits, is_signed=is_signed, init_scale=scale, use_pot_scale=True)
        x = torch.Tensor([1.4]).requires_grad_()
        y = q(x)
        y.backward()

        # check grad of parameter scale_wt by manually calculating it with numpy
        expected_scale_grad = calc_log2_scale_grad(x.detach().numpy(), scale, bits, is_signed, use_pot_scale=True)
        assert torch.isclose(expected_scale_grad, q.scale_wt.grad)


class TestFreezeStabilizedQuantizationScales:

    def test_no_pot_scale(self):
        # if use_pot_scale=False, no channels/layers can be frozen automatically
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True),
                FakeLinearTrainedQuantization(per_channel=False),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(0, 1, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria
        model[1].scale_wt_grad_ema = torch.Tensor([0.1])
        model[1].scale_wt_ema = model[1].scale_wt.clone()   # all match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 0
        assert num_frozen_scales == 1
        assert num_scales == 5

    def test_per_channel_all_stabilized(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(1, 0, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 2
        assert frozen_scales_list[0][0] == '0'
        assert torch.all(frozen_scales_list[0][2] == torch.BoolTensor([False, False, False, True]))
        assert torch.all(frozen_scales_list[1][2] == torch.BoolTensor([False, False, True, False]))
        assert num_frozen_scales == 3
        assert num_scales == 4

    def test_per_channel_all_stabilized_2(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(1, 0, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 4)

        assert len(frozen_scales_list) == 3
        assert frozen_scales_list[0][0] == '0'
        assert torch.all(frozen_scales_list[0][2] == torch.BoolTensor([False, False, False, True]))
        assert torch.all(frozen_scales_list[1][2] == torch.BoolTensor([False, False, True, False]))
        assert torch.all(frozen_scales_list[2][2] == torch.BoolTensor([False, True, False, False]))
        assert num_frozen_scales == 4
        assert num_scales == 4

    def test_per_channel_not_all_stabilized(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(0, 1, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria
        model[0].scale_wt_ema[1] += torch.tensor(1.0)       # except for the second one

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 2
        assert frozen_scales_list[0][0] == '0'
        assert torch.all(frozen_scales_list[0][2] == torch.BoolTensor([False, False, True, False]))
        assert torch.all(frozen_scales_list[1][2] == torch.BoolTensor([False, False, False, True]))
        assert num_frozen_scales == 3
        assert num_scales == 4

    def test_per_channel_and_per_layer(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                FakeLinearTrainedQuantization(per_channel=False, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(0, 1, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria
        model[1].scale_wt_grad_ema = torch.Tensor([0.1])
        model[1].scale_wt_ema = model[1].scale_wt.clone()   # all match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 1
        assert frozen_scales_list[0][0] == '1'
        assert frozen_scales_list[0][2] == torch.BoolTensor([True])
        assert num_frozen_scales == 2
        assert num_scales == 5

    def test_per_channel_and_per_layer_2(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                FakeLinearTrainedQuantization(per_channel=False, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(0, 1, 4)
        model[0].scale_wt_ema = model[0].scale_wt.clone()   # all match stabilized criteria
        model[1].scale_wt_grad_ema = torch.Tensor([0.5])
        model[1].scale_wt_ema = model[1].scale_wt.clone()   # all match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 2
        assert frozen_scales_list[0][0] == '0'
        assert torch.all(frozen_scales_list[0][2] == torch.BoolTensor([False, True, False, False]))
        assert torch.all(frozen_scales_list[1][2] == torch.BoolTensor([False, False, True, False]))
        assert num_frozen_scales == 3
        assert num_scales == 5

    def test_no_candidates_found(self):
        model = torch.nn.Sequential(
                FakeLinearTrainedQuantization(per_channel=True, use_pot_scale=True),
                )
        model(torch.empty(1, 4, 2, 2))
        model[0].frozen_channels = torch.BoolTensor([True, False, False, False])
        model[0].scale_wt_grad_ema = torch.linspace(0, 1, 4)
        model[0].scale_wt_ema = (model[0].scale_wt + 1)   # non match stabilized criteria

        frozen_scales_list,  num_frozen_scales, num_scales = FakeLinearTrainedQuantization.freeze_stabilized_scales(model, 2)

        assert len(frozen_scales_list) == 0
        assert num_frozen_scales == 1
        assert num_scales == 4


class TestFakeLinearEstimatedQuantization:

    def test_repr(self):
        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, init_scale=2.0)
        assert str(q) == "FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, frozen=False, used_scales=[2.], per_channel=False, estimator=3std)"
        q = FakeLinearEstimatedQuantization(num_bits=5, is_signed=False, init_scale=2.3)
        assert str(q) == "FakeLinearEstimatedQuantization(num_bits=5, is_signed=False, frozen=False, used_scales=[2.3], per_channel=False, estimator=3std)"

    def test_frozen(self):
        q = FakeLinearEstimatedQuantization(FakeLinearSymmetricQuantCore(8))
        assert q.frozen == False

        q.frozen = True
        assert q.frozen == True

    def test_estimate_klj_small_num_bins(self):
        torch.manual_seed(0)
        data = torch.randn((10, 10, 1))

        q = FakeLinearEstimatedQuantization(num_bits=8, is_signed=True, estimator='klj')
        scale = q.estimate_klj(data)

        assert torch.isclose(scale, torch.max(torch.abs(data)))

    def test_estimate_klj(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5))

        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, estimator='klj')
        scale = q.estimate_klj(data)

        assert torch.isclose(scale, torch.tensor(2.6378), rtol=1e-4)

    def test_forward_estimate_max(self):
        torch.manual_seed(0)
        data = torch.randn((3, 3))

        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, estimator='max', use_pot_scale=True)
        out = q(data)

        assert torch.isclose(torch.max(out), torch.Tensor([1.5]))

    def test_forward_per_channel_dynamic_init(self):
        torch.manual_seed(0)
        data = torch.randn((3, 3))

        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, per_channel=True, init_scale=2.0, estimator='max')

        assert q.scale_wt is None

        q(data)
        assert q.scale_wt.shape == torch.Size([3])

    def test_forward_per_channel_estimate_max(self):
        data = torch.Tensor([[[[5.0, -3.0],
                              [10.0, -8.3]],

                             [[1.5, -1.6],
                              [0.3, -0.5]]]])

        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, per_channel=True, estimator='max')
        q(data)

        assert torch.allclose(q.scale_wt, torch.log2(torch.Tensor([10.0, 1.6])))

    def test_forward_estimate_max_ema(self):
        torch.manual_seed(0)
        # Check that EMA is working
        data1 = torch.randn((3, 3))
        data2 = torch.randn((3, 3))
        data3 = torch.randn((3, 3))

        ema_decay = 0.99
        q = FakeLinearEstimatedQuantization(num_bits=4, is_signed=True, estimator='max', ema_decay=ema_decay)

        q(data1)
        scale_ema1 = q.scale_wt
        q(data2)
        scale_ema2 = q.scale_wt
        q(data3)
        scale_ema3 = q.scale_wt

        expected_scale_ema1 = torch.log2(torch.max(torch.abs(data1)))
        assert torch.isclose(scale_ema1, expected_scale_ema1)

        expected_biased_scale_ema1 = expected_scale_ema1 * (1 - ema_decay)
        expected_biased_scale_ema2 = expected_biased_scale_ema1 * ema_decay + torch.log2(torch.max(torch.abs(data2))) * (1 - ema_decay)
        expected_scale_ema2 = expected_biased_scale_ema2 / (1 - ema_decay**2)
        assert torch.isclose(scale_ema2, expected_scale_ema2)

        expected_biased_scale_ema3 = expected_biased_scale_ema2 * ema_decay + torch.log2(torch.max(torch.abs(data3))) * (1 - ema_decay)
        expected_scale_ema3 = expected_biased_scale_ema3 / (1 - ema_decay**3)
        assert torch.isclose(scale_ema3, expected_scale_ema3)

    def test_no_power_of_two(self):
        q = FakeLinearEstimatedQuantization(estimator='max', ema_decay=0.0)
        x = torch.Tensor([1.4])
        res = q(x)
        assert torch.isclose(q.scale, torch.Tensor([1.4]))
        assert torch.isclose(res, torch.Tensor([1.3891]), rtol=1e-4)


class TestFakeLinearPPQuantization:

    def test_repr(self):
        q = FakeLinearPPQuantization(num_bits=4, is_signed=True)
        assert str(q) == "FakeLinearPPQuantization(num_bits=4, is_signed=True, frozen=False, used_scales=[1.])"

    def test_frozen(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5))

        q = FakeLinearPPQuantization(num_bits=4, is_signed=True, init_scale=1.0, ema_decay=0.0, iterations=1)
        q.frozen = True

        res = q(data)

        assert torch.isclose(q.scale, torch.Tensor([1.0]))

    def test_single_iteration(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5))

        init_scale = torch.max(torch.abs(data)).item()
        q = FakeLinearPPQuantization(num_bits=4, is_signed=True, init_scale=init_scale, ema_decay=0.0, iterations=1)

        res = q(data)
        assert torch.isclose(q.scale, torch.Tensor([4.443195]))

    def test_multiple_iterations(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5))

        init_scale = torch.max(torch.abs(data)).item()
        q = FakeLinearPPQuantization(num_bits=4, is_signed=True, init_scale=init_scale, ema_decay=0.0, iterations=3)

        res = q(data)
        assert torch.isclose(q.scale, torch.Tensor([4.226239]))

    def test_single_iteration_ema(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5))

        init_scale = torch.max(torch.abs(data)).item()
        q = FakeLinearPPQuantization(num_bits=4, is_signed=True, init_scale=init_scale, ema_decay=0.9, iterations=1)

        res = q(data)
        res = q(data)
        res = q(data)

        assert torch.isclose(q.scale, torch.Tensor([4.3421]))

    def test_gradient_propagation(self):
        torch.manual_seed(0)
        data = torch.randn((100, 100, 5)).requires_grad_()

        q = FakeLinearPPQuantization(num_bits=4, is_signed=True)

        res = q(data)

        assert res.requires_grad
        assert not q.scale_wt.requires_grad


#class TestFakePOTPPQuantization:
#
#    def test_repr(self):
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True))
#        assert str(q) == "FakePOTPPQuantization(quant_core=FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), frozen=False, used_scales=[1.], per_channel=False)"
#
#    def test_frozen(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5))
#
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_decay=0.0, init_scale=1.0, iterations=20)
#        q.frozen = True
#
#        res = q(data)
#
#        assert torch.isclose(q.scale, torch.Tensor([1.0]))
#
#    def test_increasing_scale(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5))
#
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_decay=0.0, init_scale=1.0, iterations=4)
#
#        res = q(data)
#        assert torch.isclose(q.scale, torch.Tensor([4.0]))
#
#    def test_decreasing_scale(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5))
#
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_decay=0.0, init_scale=16.0, iterations=4)
#
#        res = q(data)
#        assert torch.isclose(q.scale, torch.Tensor([4.0]))
#
#    def test_gradient_propagation(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5)).requires_grad_()
#
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True))
#
#        res = q(data)
#
#        assert res.requires_grad
#        assert not q.scale_wt.requires_grad
#
#    def test_freeze_stabilized_scales(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5))
#
#        q = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_scale_decay=0.99, init_scale=16.0, iterations=10)
#
#        for i in range(10):
#            res = q(data)
#
#        frozen_scales, num_frozen_scales, num_scales = FakePOTPPQuantization.freeze_stabilized_scales(q)
#        assert len(frozen_scales) == 1
#        assert num_frozen_scales == 1
#
#        res = q(data)
#        frozen_scales, num_frozen_scales, num_scales = FakePOTPPQuantization.freeze_stabilized_scales(q)
#        assert len(frozen_scales) == 0
#        assert num_frozen_scales == 1
#
#    def test_freeze_stabilized_scales_multiple_quantizers(self):
#        torch.manual_seed(0)
#        data = torch.randn((100, 100, 5))
#
#        q1 = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_scale_decay=0.99, init_scale=16.0, iterations=1)
#        q2 = FakePOTPPQuantization(FakeLinearSymmetricQuantCore(num_bits=4, is_signed=True), ema_scale_decay=0.99, init_scale=1.0, iterations=1)
#        model = torch.nn.Sequential(q1, nn.Conv2d(1, 1, 1), q2)
#
#        for i in range(10):
#            q1(data)
#            q2(data)
#
#        frozen_scales, num_frozen_scales, num_scales = FakePOTPPQuantization.freeze_stabilized_scales(model)
#        assert len(frozen_scales) == 1
#        assert num_frozen_scales == 1
#
#        frozen_scales, num_frozen_scales, num_scales = FakePOTPPQuantization.freeze_stabilized_scales(model)
#        assert len(frozen_scales) == 1
#        assert num_frozen_scales == 2
#
#        assert torch.isclose(q1.scale, torch.Tensor([4.0]))
#        assert torch.isclose(q2.scale, torch.Tensor([4.0]))

class TestFakeNonLinearQEMQuantization:

    def test_repr(self):
        q = FakeNonLinearQEMQuantization(num_bits=4)
        assert str(q) == "FakeNonLinearQEMQuantization(frozen=False, scale=1.0, q_levels=[-128. -111.  -94. ...  93. 110. 127.])"

    def test_quant_result(self):
        q = FakeNonLinearQEMQuantization(num_bits=4)
        x = torch.Tensor([0.45])
        res = q(x)
        assert torch.isclose(res, torch.Tensor([0.4609375]))

    def test_q_level_update(self):
        torch.manual_seed(0)
        q = FakeNonLinearQEMQuantization(num_bits=4, q_level_ema_decay=0.0)
        x = torch.randn(1, 3, 5, 5)
        initial_q_levels = q.q_levels
        res = q(x)
        updated_q_levels = q.q_levels
        assert not torch.allclose(initial_q_levels, updated_q_levels)

    def test_dont_update_q_levels_during_evaluation(self):
        q = FakeNonLinearQEMQuantization(num_bits=4, q_level_ema_decay=0.0)
        x = torch.Tensor([0.45])
        q.eval()
        initial_q_levels = q.q_levels
        res = q(x)
        updated_q_levels = q.q_levels
        assert torch.allclose(initial_q_levels, updated_q_levels)

    def test_dont_update_q_levels_when_frozen(self):
        q = FakeNonLinearQEMQuantization(num_bits=4, q_level_ema_decay=0.0)
        x = torch.Tensor([0.45])
        q.frozen = True
        initial_q_levels = q.q_levels
        res = q(x)
        updated_q_levels = q.q_levels
        assert torch.allclose(initial_q_levels, updated_q_levels)

    def test_calculate_q_levels(self):
        q = FakeNonLinearQEMQuantization(num_bits=3)
        x = torch.linspace(-1, 0.7143, 7)
        res = q(x)
        # NOTE: q levels that get no input value are set to zero
        assert torch.allclose(q.q_levels, torch.Tensor([-127.9999,  -91.4282,  -54.8565,  -18.2848,   18.2869,   54.8586,  91.4303,    0.0000]), atol=1e-4)

    def test_dims(self):
        q = FakeNonLinearQEMQuantization(num_bits=3)
        x = torch.randn(1, 2, 3, 3)
        res = q(x)
        assert res.shape == torch.Size([1, 2, 3, 3])

    def test_freeze_stabilized_q_levels_stabilization_criteria(self):
        # if use_pot_scale=False, no channels/layers can be frozen automatically
        model = torch.nn.Sequential(
                FakeNonLinearQEMQuantization(num_bits=4),
                FakeNonLinearQEMQuantization(num_bits=4),
                )
        model[0].q_levels_stab = torch.linspace(-126, 126, 2**4)    # different values as q_levels, so stabilization criteria is not met
        model[1].q_levels_stab = torch.linspace(-128, 127, 2**4)    # same values as q_levels, so stabilization criteria is met

        frozen_quantizers, num_frozen_quantizers, num_quantizers = FakeNonLinearQEMQuantization.freeze_stabilized_q_levels(model)

        assert num_quantizers == 2
        assert num_frozen_quantizers == 1
        assert len(frozen_quantizers) == 1
        assert frozen_quantizers[0] == '1'

    def test_freeze_stabilized_q_levels_freeze_q_levels_smallest_round_error(self):
        # if use_pot_scale=False, no channels/layers can be frozen automatically
        model = torch.nn.Sequential(
                FakeNonLinearQEMQuantization(num_bits=4),
                FakeNonLinearQEMQuantization(num_bits=4),
                )
        model[0].q_levels_stab = torch.linspace(-128, 127, 2**4)        # stabilization criteria is met
        model[1].q_levels_stab = torch.linspace(-127.5, 126.6, 2**4)    # stabilization criteria is met

        frozen_quantizers, num_frozen_quantizers, num_quantizers = FakeNonLinearQEMQuantization.freeze_stabilized_q_levels(model)

        assert num_quantizers == 2
        assert num_frozen_quantizers == 1
        assert len(frozen_quantizers) == 1
        assert frozen_quantizers[0] == '0'

    def test_freeze_stabilized_q_levels_check_if_q_levels_are_rounded_when_frozen(self):
        # if use_pot_scale=False, no channels/layers can be frozen automatically
        model = torch.nn.Sequential(
                FakeNonLinearQEMQuantization(num_bits=4),
                )
        model[0].q_levels_stab = torch.linspace(-127.5, 126.6, 2**4)    # stabilization criteria is met

        frozen_quantizers, num_frozen_quantizers, num_quantizers = FakeNonLinearQEMQuantization.freeze_stabilized_q_levels(model)

        assert num_quantizers == 1
        assert num_frozen_quantizers == 1

        assert torch.allclose(model[0].q_levels, torch.linspace(-128, 127, 2**4))


class TestAIWQ:

    def test_repr(self):
        def param_module_fn(x, w):
            pass

        aiwq = AIWQ(param_module_fn)

        aiwq.metric = torch.tensor(1.234)
        assert str(aiwq) == "AIWQ(metric=1.234)"

        aiwq.metric = torch.tensor(1234.5)
        assert str(aiwq) == "AIWQ(metric=1.234e+03)"

    def test_kl_different_weights(self):
        param_module_fn = lambda x, w: x * w

        aiwq = AIWQ(param_module_fn)
        prev_weight = 0.9
        weights = 1.1
        input = torch.Tensor([[[1.0, 1.1], [1.2, 1.3]]])
        output = param_module_fn(input, weights)

        aiwq.prev_weight = prev_weight
        aiwq(input, output)

        assert torch.isclose(aiwq.metric, torch.tensor(6.626494884490967))

    def test_kl_same_weights(self):
        param_module_fn = lambda x, w: x * w

        aiwq = AIWQ(param_module_fn)
        prev_weight = 0.9
        weights = 0.9
        input = torch.Tensor([[[1.0, 1.1], [1.2, 1.3]]])
        output = param_module_fn(input, weights)

        aiwq.prev_weight = prev_weight
        aiwq(input, output)

        assert torch.isclose(aiwq.metric, torch.tensor(0.0))

    def test_zero_inputs(self):
        param_module_fn = lambda x, w: x * w

        aiwq = AIWQ(param_module_fn)
        prev_weight = 0.9
        weights = 1.1
        input = torch.zeros(1, 2, 2)
        output = param_module_fn(input, weights)

        aiwq.prev_weight = prev_weight
        aiwq(input, output)

        assert torch.isclose(aiwq.metric, torch.tensor(0.0))

    def test_no_prev_weight_yet(self):
        def param_module_fn(x, w):
            raise AssertionError("The parameter function should not be called!")

        aiwq = AIWQ(param_module_fn)
        input = torch.ones(1, 2, 2)
        output = torch.ones(1, 2, 2)
        aiwq(input, output)

        assert aiwq.metric == 0.0

    def test_ema(self):
        param_module_fn = lambda x, w: x * w

        aiwq = AIWQ(param_module_fn)
        input1 = torch.Tensor([[[1.0, 1.1], [1.2, 1.3]]])
        input2 = input1 + 1
        input3 = input1
        prev_weight = 0.9
        weights = 1.1
        output1 = param_module_fn(input1, weights)
        output2 = param_module_fn(input2, weights)
        output3 = param_module_fn(input3, weights)

        aiwq.prev_weight = prev_weight
        aiwq(input1, output1)
        assert torch.isclose(aiwq.metric, torch.tensor(6.626494884490967))

        aiwq(input2, output2)
        assert torch.isclose(aiwq.metric, torch.tensor(14.778736946760718))

        aiwq(input3, output3)
        assert torch.isclose(aiwq.metric, torch.tensor(12.058603699467795))


