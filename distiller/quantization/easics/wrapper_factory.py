from .wrappers import *
from .layers import FakeLinearEstimatedQuantization, FakeLinearConstQuantization


class AbstractFactory:

    def __init__(self, adjacency_map):
        """
        adjacency_map: Map with interconnections created by SummaryGraph
        """
        self.adjacency_map = adjacency_map

    def __call__(self, module, name, qbits_map):
        """
        module (nn.Module): Module to be wrapped
        name (str):         Name of the module
        qbits_map (dist):   Map with the number of activation, weight and bias bits for each layer
                            in the network
        """
        raise NotImplementedError

    def requires_output_quant(self, type_name, type_name_list=['Add', 'Concat', 'Relu', 'Clip', 'LeakyRelu', 'BatchNormalization']):
        """If one of the successors is not within the type_name_list, return True
        Also return True in case there are no successors, which means this is the output layer which we want to
        be quantized.
        All layers types in the type_name_list do not require their input to be quantized
        NOTE: Clip refers to ReLU6
        """
        if len(self.adjacency_map[type_name].successors) == 0:
            return True

        for s in self.adjacency_map[type_name].successors:
            if s.type not in type_name_list:
                return True

        return False


class QConvLinearFactory(AbstractFactory):

    def __init__(self, adjacency_map, weight_quant_layer_cls, bias_quant_layer_cls, act_quant_layer_cls):

        super(QConvLinearFactory, self).__init__(adjacency_map)
        self.weight_quant_layer_cls = weight_quant_layer_cls
        self.bias_quant_layer_cls = bias_quant_layer_cls
        self.act_quant_layer_cls = act_quant_layer_cls

    def _is_output_producing_conv_or_linear(self, name):
        """Return True if this conv/linear layer is an output producing layer
        """
        if len(self.adjacency_map[name].successors) == 0:
            return True

        # recursively search successors
        for s in self.adjacency_map[name].successors:

            # if we find another conv/linear layer, this is not the last one so return False
            if s.type in ['Conv', 'Gemm']:
                return False

            if self._is_output_producing_conv_or_linear(s.name):
                return True

        return False

    def _create_quantizers(self, name, qbits_map, postfix=''):

        bits_weights = qbits_map[name].wts
        bits_bias = qbits_map[name].bias
        bits_output = qbits_map[name].acts

        # in case this is a network output producing conv/linear layer, force per layer quantization
        # rather than per channel quantization
        kwargs = {}
        if self._is_output_producing_conv_or_linear(name + postfix):
            kwargs['per_channel'] = False

        quant_weights = quant_bias = quant_output = None
        if bits_weights is not None:
            quant_weights = self.weight_quant_layer_cls(num_bits=bits_weights, is_signed=True, per_channel_dim=0, **kwargs)
        if bits_bias is not None:
            quant_bias = self.bias_quant_layer_cls(num_bits=bits_bias, is_signed=True, **kwargs)
        if bits_output is not None and self.requires_output_quant(name + postfix):
            quant_output = self.act_quant_layer_cls(num_bits=bits_output, is_signed=True, **kwargs)

        return quant_weights, quant_bias, quant_output

    def __call__(self, module, name, qbits_map):
        quants = self._create_quantizers(name, qbits_map)
        return QConvLinear(module, *quants)


class QConvLinearFoldedBnFactory(QConvLinearFactory):

    def __init__(self, adjacency_map, weight_quant_layer_cls, bias_quant_layer_cls, act_quant_layer_cls, freeze_bn=False):
        super(QConvLinearFoldedBnFactory, self).__init__(adjacency_map,
                                                         weight_quant_layer_cls,
                                                         bias_quant_layer_cls,
                                                         act_quant_layer_cls)
        self.freeze_bn = freeze_bn

    def __call__(self, module, name, qbits_map):
        quants = self._create_quantizers(name, qbits_map, ".1")
        return QConvLinearFoldedBn(module[0], module[1], self.freeze_bn, *quants)


class QReluFactory(AbstractFactory):

    def __init__(self, adjacency_map, act_quant_layer_cls):
        super(QReluFactory, self).__init__(adjacency_map)
        self.act_quant_layer_cls = act_quant_layer_cls

    def __call__(self, module, name, qbits_map):
        bits_activations = qbits_map[name].acts
        quant_activations = None
        if bits_activations is not None:
            quant_activations = self.act_quant_layer_cls(num_bits=bits_activations, is_signed=False)
        return QRelu(module, quant_activations)


class QLeakyReLUFactory(AbstractFactory):

    def __init__(self, adjacency_map, act_quant_layer_cls):
        super(QLeakyReLUFactory, self).__init__(adjacency_map)
        self.act_quant_layer_cls = act_quant_layer_cls

    def __call__(self, module, name, qbits_map):
        bits_activations = qbits_map[name].acts
        quant_activations = None
        quant_negative_slope = None
        if bits_activations is not None:
            # EASICS uses 18 bits with 2 bits on the left of the decimal point for the negative slope and represents alpha 8 times larger
            # This corresponds to scale factor 2 (for the 2 bits) divided by 8 (for the alpha*8) = 2 / 8 = 1 / 4
            quant_negative_slope = FakeLinearConstQuantization(scale=1/4., num_bits=18, is_signed=True)
            quant_activations = self.act_quant_layer_cls(num_bits=bits_activations, is_signed=True)
        return QLeakyReLU(module, None, quant_activations, quant_negative_slope)


class QEltwiseAddFactory(AbstractFactory):

    def __init__(self, adjacency_map, act_quant_layer_cls):
        super(QEltwiseAddFactory, self).__init__(adjacency_map)
        self.act_quant_layer_cls = act_quant_layer_cls

    def __call__(self, module, name, qbits_map):
        bits_activations = qbits_map[name].acts
        quant_input = quant_output = None
        if bits_activations is not None:
            quant_input = self.act_quant_layer_cls(num_bits=bits_activations, is_signed=True)
            if self.requires_output_quant(name):
                quant_output = self.act_quant_layer_cls(num_bits=bits_activations, is_signed=True)
        return QEltwiseAdd(module, quant_input, quant_output)


class QEltwiseConcatFactory(AbstractFactory):

    def __init__(self, adjacency_map, act_quant_layer_cls):
        super(QEltwiseConcatFactory, self).__init__(adjacency_map)
        self.act_quant_layer_cls = act_quant_layer_cls

    def __call__(self, module, name, qbits_map):
        bits_activations = qbits_map[name].acts
        quant_output = None
        if bits_activations is not None:
            quant_output = self.act_quant_layer_cls(num_bits=bits_activations, is_signed=True)
        return QEltwiseConcat(module, quant_output)


class QAvgPoolFactory(AbstractFactory):

    def __init__(self, adjacency_map, act_quant_layer_cls):
        super(QAvgPoolFactory, self).__init__(adjacency_map)
        self.act_quant_layer_cls = act_quant_layer_cls

    def __call__(self, module, name, qbits_map):
        bits_output = qbits_map[name].acts
        quant_recip = quant_output = None
        if bits_output is not None:
            # The reciprocal is constant and depends on kernel size only. Consequently, its scale factor is also constant.
            # However, since it is not possible to estimate the kernel size at this point in the case we are wrapping an AdaptiveAvgPool2d,
            # which requires a forward pass, we insert a quantizer with max estimator to determine the scale factor instead of inserting
            # a quantizer with constant scale factor.
            quant_recip = FakeLinearEstimatedQuantization(estimator='max', use_pot_scale=True, num_bits=bits_output, is_signed=True)
            if self.requires_output_quant(name):
                # if previous layer was relu or relu6, set is_signed=False
                is_signed = not self.adjacency_map[name].predecessors[0].type in ['Relu', 'Clip']
                quant_output = self.act_quant_layer_cls(per_channel=False, num_bits=bits_output, is_signed=is_signed)
        return QAvgPool(module, quant_recip, quant_output)
