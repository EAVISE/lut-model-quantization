import os
import lightnet as ln
import torch
import distiller
from distiller.utils import set_model_input_shape_attr
from distiller.quantization import FakeNonLinearQEMQuantization, FakeLinearTrainedQuantization
from utils import load_checkpoint
from functools import partial

import lightnet_examples.models

__all__ = ['params']

DATA_ROOT = 'data/ms_coco'

def params(weight_file=[None]):
    """Returns a generator that creates a new network each iteration with different hyper parameters
    """
    with open(os.path.join(DATA_ROOT, 'metadata/coco.names')) as f:
        class_label_map = [line.strip() for line in f]

    ln.network.layer.Conv2dBatchReLU.__repr__ = torch.nn.Module.__repr__ # reset custom repr from lightnet for visualization purposes

    bits_weights          = [4,   ]
    freeze_bn_start       = [0, ]
    thresh_lrs            = [1e-2,]
    base_lr = 1e-6

    use_pot_scale = True
    for i in range(len(bits_weights)):

        max_batches = 100000
        _params = ln.engine.HyperParameters(
            # Network
            class_label_map = class_label_map,
            input_dimension = (416, 416),
            batch_size = 32,
            _mini_batch_size = 32,
            max_batches = max_batches,

            # Dataset
            _train_set = os.path.join(DATA_ROOT, 'metadata/trainvalno5k.part'),
            _valid_set = os.path.join(DATA_ROOT, 'metadata/5k.part'),

            # Preprocessing
            preprocessor_train = 'yolo_aug',
            preprocessor_test = 'yolo_noaug',

            # Quantization
            quantize_inputs = True,
            bits_inputs = 8,
            signed_inputs = True,
            bits_activations = 8,
            bits_weights = 8,
            bits_bias = None,
            weight_quant_layer_cls=partial(FakeNonLinearQEMQuantization, num_bits_q_levels=8, q_level_ema_decay=0.0, q_level_stab_ema_decay=0.999),
            act_quant_layer_cls=partial(FakeLinearTrainedQuantization, use_pot_scale=True),
            freeze_thresholds_start = 1000,
            freeze_thresholds_period = 50,
            freeze_bn_start = 50000,
            freeze_q_levels_start = 1000,
            freeze_q_levels_period = 50,
            freeze_all_q_levels = 3/4 * max_batches,
            profit_percent = 1.0,
        )

        # Set stage specific quantizer parameters
        _params.bits_weights = bits_weights[i]
        _params.freeze_bn_start = freeze_bn_start[i]

        # Construct network
        _params.network = lightnet_examples.models.TinyYoloV2(
            len(_params.class_label_map),
            anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)],
            voc=False
        )
        set_model_input_shape_attr(_params.network, input_shape=(1, 3, *_params.input_dimension))

        # quantize network
        quantizer = distiller.quantization.EasicsQuantizer(_params.network,
                                                           bits_activations=_params.bits_activations,
                                                           bits_weights=_params.bits_weights,
                                                           bits_bias=_params.bits_bias,
                                                           quantize_inputs=_params.quantize_inputs,
                                                           bits_inputs=_params.bits_inputs,
                                                           signed_inputs=_params.signed_inputs,
                                                           weight_quant_layer_cls=_params.weight_quant_layer_cls,
                                                           act_quant_layer_cls=_params.act_quant_layer_cls)
        quantizer.prepare_model()

        # Load initial weights
        if weight_file[0] is not None:
            load_checkpoint(_params.network, weight_file[0])

        # Loss
        _params.loss = ln.network.loss.RegionLoss(
            len(_params.class_label_map),
            _params.network.anchors,
            _params.network.stride,
        )

        # Postprocessing
        _params._post = ln.data.transform.Compose([
            ln.data.transform.GetDarknetBoxes(0.001, _params.network.stride, _params.network.anchors),
            ln.data.transform.NMS(0.5),
            ln.data.transform.TensorToBrambox(_params.class_label_map),
        ])

        # create parameter groups
        if _params.profit_percent < 1.0:
            weight_parameters = {'params': distiller.quantization.QConvLinear.stable_parameters(_params.network, _params.profit_percent)}
        else:
            weight_parameters = {'params': [v for k, v in _params.network.named_parameters() if 'scale_wt' not in k and 'q_levels' not in k]}
        thresh_parameters = {'params': [v for k, v in _params.network.named_parameters() if 'scale_wt' in k]}

        # Optimizer for weights
        _params.optimizer_weights = torch.optim.Adam(
            [weight_parameters],
            lr = base_lr,
        )

        # Optimizer for thresholds
        _params.optimizer_thresh = torch.optim.Adam(
            [thresh_parameters],
            lr = thresh_lrs[i],
        )

        # Scheduler for weights
        _params.scheduler_weights = ln.engine.SchedulerCompositor(
        #   batch   scheduler
            (0,     torch.optim.lr_scheduler.CosineAnnealingLR(_params.optimizer_weights, _params.max_batches))
        )

        # Scheduler for thresholds
        _params.scheduler_thresh = ln.engine.SchedulerCompositor(
        #   batch   scheduler
            (0,     torch.optim.lr_scheduler.StepLR(_params.optimizer_thresh, 1000, gamma=0.5))
        )

        yield _params
